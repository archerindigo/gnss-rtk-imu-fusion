% ----------------------------------------------------------------------------
%                               !Theoretical Background
% ----------------------------------------------------------------------------
\chapter{Theoretical Background}
\label{Chapter::Theoretical_Background} % Outline text

% Background topics that are necessary to understand your thesis
\section{Principle of GNSS and RTK}
\label{Section::Principle_GNSS_RTK}

    \acrfull{RTK} is a method to correct several types of error in a global navigation satellite system (\acrshort{GNSS}). It is essentially a combination of differential GNSS positioning and carrier-phase measurement to enhance the accuracy of real-time positioning up to centimeter level, and precision up to millimeter level. To understand how the accuracy and precision are achieved, the principle of ordinary GNSS positioning method and the problems behind have to be reviewed.

\subsection{Code Differential Positioning}
\label{Section::Principle_GNSS_RTK::Code_Differential}

    Ordinary GNSS positioning is based on code differential positioning, which relies on tracking the time of travel of the satellite signals. Satellites in the space continuously broadcast signal modulated with ranging code and navigation code. The ranging code is a series of pseudorandom noise (PRN) for identifying the satellite, while the navigation code provides information such as the orbit of the satellites and the satellite's clock time. GNSS receivers generate and match the PRN through a delay lock loop (DLL) in order to determine the transmission and reception time of the signal from a specific satellite to the receiver. By multiplying the time of travel of the signal and the speed of the electromagnetic signal (i.e. the speed of light), the pseudorange $R_{p,r}^s$ between a certain satellite $s$ and a receiver $r$ can be resolved. The equation is expressed as:
    
    \begin{equation}\label{eq_pseudorange}
        R_{p,r}^s = c \cdot (t_r - t_s) + \epsilon
    \end{equation}
    
    where, $t_r$ is the receiver's clock time when the signal is received and $t_s$ is the signal transmission time. $c$ is the speed of light in vacuum.
    
    The range is called pseudo because the measurement is affected by multiple forms of error, which is abstracted as $\epsilon$. The errors will further be described in the next subsection.
    
    Location of the receiver is then calculated by trilateration. For a 3D space, range measurements from at least 4 satellites are required to resolve the position of the receiver and as well as to correct the receiver's clock offset. Figure \ref{fig::trilateration} gives an illustration with more details.

    \begin{figure}[hbt!]
        \centering
        \includegraphics[width=0.5\textwidth]{theory/trilateration.png}
        \caption[Illustration of trilateration with 3 satellites]{Illustration of trilateration with 3 satellites. With ideal range measurements (dotted circles), the 3 circles would intersect at one position at the green dot (in 3D case, the 3 spheres would give 2 intersections instead, with only one of them locates reasonably). However, error in time of travel measurement makes the position uncertain. Thus, the fourth satellite is required to allow a single intersection to be computed under a corrected time variable.}
        \label{fig::trilateration}
    \end{figure}

\subsection{Source of Errors}
\label{Section::Principle_SGNSS_RTK::Errors}

    Since code differential positioning relies on measuring the Time-of-Flight (ToF) of the signal, it exposes to multiple sources of error \cite{subirana_5_2013}. One type of errors is related to atmospheric effects, such as ionosphere error and troposphere error. They affect the average propagation speed of the signal wave in different level of the space, so it takes longer for the wave to reach the receiver, increasing the measured range.
    
    Satellite clock bias is another error from satellites which will lead to deviation in determining the satellites' signal transmission time. Hence, it adds error into the range measurement.
    
    Expending $\epsilon$ in equation \eqref{eq_pseudorange} with the errors mentioned above, the range equation becomes:
    
    \begin{equation}
        R_{p,r}^s = c \cdot (t_r - t_s) + T_r^s(t) + I_r^s(t) + c \cdot dt_r^s + \epsilon_u
    \end{equation}
    
    where $T_r^s$, $I_r^s$ and $c \cdot dt_r^s$ are troposphere error, ionosphere error and satellite clock bias respectively, in units of length.
    
    These kinds of error can be compensated by using differential method (DGNSS), which will be described in the upcoming subsection.
    
    Meanwhile, there are still some errors $\epsilon_u$. They are some minor errors that are not modelled, as well as some other errors that cannot be eliminated by DGNSS, such as receiver clock bias, receiver noise, and multipath effect.
    
    Another kind of error comes from the sampling of satellite signal. Taking \acrshort{GPS} as an example, the ranging code (Coarse/acquisition code, C/A code) sent by the satellites has a chipping rate of 1.023 MHz when it is broadcasted in L1 band (1575.42 MHz). The code length corresponds to about 293 m. As a rule of thumb, the accuracy of pseudorange measurement is about 1\% of the wavelength \cite{subirana_4_2_1_2013}. It accounts for 2.93 m of error in this case. On the other hand, the carrier wave has a much shorter wavelength of 0.19 m. If a receiver can observe the exact total number of cycles of and the phase of the carrier wave, the accuracy and precision would greatly be improved to under centimeter level. This carrier phase measurement is the second main component of RTK and will briefly be described in the later subsection.

\subsection{Differential GNSS (DGNSS)}
\label{Section::Principle_GNSS_RTK::DGNSS}

    DGNSS is based on the assumption that GNSS receivers within a short baseline distance (i.e. 10 km for single band receivers) would experience similar extent of atmospheric and satellite errors. By subtracting the pseudorange with the derivation, those kinds of errors can be minimized.
    
    To obtain the derivation, an additional receiver (base station, or reference station) has to be set up at a known position and monitor the satellite signals continuously.
    
    The purely geometric, or the true range $\rho_b^s$ between the base station $b$ and a certain satellite $s$ is calculated purely by the position of both devices at a certain time:
    
    \begin{equation}
        \rho_b^s = \sqrt{(X^s - X_b)^2 + (Y^s - Y_b)^2 + (Z^s - Z_b)^2}
    \end{equation}
    
    where $[X^s, Y^s, Z^s]$ and $[X_b, Y_b, Z_b]$ are the 3-dimension coordinate of the satellite and the base station respectively.
    
    The range correction $\varDelta \rho$ is made by subtracting the pseudorange $R_b^s$ measured in the base station and the geometric range:
    
    \begin{equation}
        \varDelta \rho = R_b^s - \rho_b^s = T_b^s(t) + I_b^s(t) + c \cdot dt_b^s + \epsilon_b
    \end{equation}
    
    This correction will be distributed to the nearby client receivers (rovers). They will subtract it from their own pseudorange measurement $R_r^s$ in order to come up with a more accurate range $\rho_r^s$:
    
    \begin{equation}
        \rho_r^s = R_r^s - \varDelta \rho
    \end{equation}
    
    Note that $\rho_r^s$ still contains errors that cannot be covered in the correction data, such as receiver clock bias and multipath effect. Nevertheless, large portion of the errors are being compensated.

\subsection{Carrier Phase Measurement}
\label{Section::Principle_GNSS_RTK::Carrier_Phase}

    Besides ranging code, range between a receiver and a satellite can also be observed by measuring the length of the satellite's carrier wave. Assume there is no error, the total length $\phi_r^s$ of the wave is expressed as:
    
    \begin{equation}
        \phi_r^s = N \cdot \lambda + \lambda_j + \lambda_r
    \end{equation}
    
    where $\lambda$ is the wavelength of the carrier wave. $\lambda_j$ and $\lambda_r$ are the fractional wavelength at the satellite and the receiver side respectively.
    
    As shown in the equation, there are two challenges in the measurement. One is to resolve the fractional phase ambiguity. Another one is called integer ambiguity, which is the unknown integer number $N$ of full cycles.
    
    Resolving ambiguities is the key to obtain precise and accurate range. While the fractional ambiguity can be eliminated by double difference as DGNSS provides \cite{subirana_gnss_1_2013}, dealing with integer ambiguity is more challenging. Different approaches were proposed throughout the years. Integer rounding, integer bootstrapping, integer least square are some of them \cite{teunissen_23_2017}.
    
    To obtain good estimation, the models generally require multiple epochs of observation from multiple receivers, frequencies, and satellites. It is also required to keep track on the locked satellite signals in order to count the cycle slip. The distance between the rover and the base station, which is called baseline, can induce larger atmospheric delay parameters and so affect the convergence times and accuracy of the solution \cite{brack_long_2017}.
    
    When the set of ambiguities is fully resolved, the positioning state is called \textbf{fixed} and is regarded as the most precise. It is also possible to provide generally less precise but relative rapid to be available solutions by resolving only a subset of ambiguities. In this case, the carrier-phase measurement status is regarded as \textbf{float}.

\subsection{Transport of Correction Data}
\label{Section::Principle_GNSS_RTK::NTRIP}

    To distribute the DGNSS correction data from a base station to rovers, the traditional method is to send the data directly through radio frequency. LoRa and Wi-Fi are two common options. LoRa uses a lower frequency band at around 900 MHz, which can reach longer range. As for Wi-Fi, though it runs at higher frequency of 2.4 GHz or 5 GHz, which limits its range and likely to conflict with other communication links, it is easily available and can provide higher data rate.

    Correction data is packaged in standard format \acrshort{RTCM} version 2 or newer (published by the Radio Technical Commission for Maritime Services), along with some other additional information from reference stations \cite{noauthor_rtcm_nodate}.

    It is also possible to transfer the data through the Internet.  For this, the \acrfull{NTRIP} was released \cite{weber_networked_2005}. NTRIP provides a convenient way for people to access nearby base station(s) through Internet, instead of setting-up a base station and the full data path on their own.
    
    In the experiment done in this thesis, the NTRIP topology was used since the base station used is a commercial station provided by a third party.

\section{Geographic Coordinate Systems}

    There are a number of coordinate systems to describe the geographic position on the Earth. Each of them has its advantages in different scenarios. This section briefly describes the coordinate systems that are involved in this thesis. Figure \ref{fig::coordinate_relationships} also illustrates those coordinate systems.

\subsection{Geodetic Coordinates}

    Geodetic coordinates are the type of curvilinear orthogonal coordinate system that widely be used in daily life. They include latitude (south/ north), longitude (east/ west) and altitude (height above ellipsoid). Latitude and longitude are usually represented in degrees ranging from −180$^{\circ}$ to +180$^{\circ}$, while altitude is represented in meters. This set of coordinates is referred as \textbf{LLA} in this thesis.
    
    An ellipsoid model has to be chosen as reference for this coordinate system. The World Geodetic System 1984 (WGS84) \cite{noauthor_nga_nodate}, which is the common standard in geodesy, is being used.
    
    Since the coordinates refer to a curvilinear surface, calculating the distance between two positions in geodetic coordinates is more complex than in the Cartesian coordinates. Generally, the haversine formula would be applied to compute the great-circle distance. Also, it is not intuitive to illustrate the distance when plotting the positions using this system. Therefore, an alternative system has to be chosen.

\subsection{North-East-Down Coordinate System}

    \acrfull{NED} is a local coordinate system which based on a horizontal tangent plane defined by a certain geodetic position. It is also a Cartesian coordinate system where the x-axis points towards north, y-axis points towards east and z-axis points towards the center of the Earth. The origin $(0, 0, 0) \text{m}$ of the tangent plane is located at the chosen geodetic position.
    
    As the axes of the system reflects the cardinal directions, as well as the value of the coordinates are small compare to ECEF, NED is more convenient to visualize the positions.

\subsection{Earth-centered, Earth-fixed Coordinate System}

    \gls{ECEF} is another Cartesian spatial reference system in which the origin is set at the center of the Earth, x-, y-axis points towards 0$^{\circ}$ and 90$^{\circ}$ longitude respectively in the plane of the equator, and z-axis points towards the North Pole. The unit of the coordinates is in meters.

    Although linear distance is easier to compute using this Cartesian coordinate system, the relationship between positions is not intuitively presented due to the alignment of the axes. Nevertheless, ECEF is a coordinate system that can be converted from LLA without the need to set-up a reference position as NED required, making it more convenient when only global coordinates are involved in the analysis.

    \begin{figure}[hbt!]
        \centering
        \includegraphics[width=0.4\textwidth]{theory/ECEF_ENU_Longitude_Latitude_relationships.png}
        \caption[Relationships between ECEF, ENU, Longitude and Latitude]{An illustration of Longitude, Latitude, ECEF, and ENU (East-North-Up) Coordinates (adapted from Wikipedia \cite{noauthor_ecef_2010}). NEU coordinate is similar to ENU except for swapping 'down' for 'up' and x for y. ECEF and NEU are Cartesian coordinate systems which are more intuitive to illustrate positions on a graph and simpler to compute the linear distance.}
        \label{fig::coordinate_relationships}
    \end{figure}

\section{Error State Kalman Filter for Sensor Fusion}

    \acrfull{ESKF} is a variant of the Kalman Filter for non-linear systems. Unlike other Kalman Filter such as \acrfull{EKF}, ESKF estimates the error state to correct the state estimated from sensor measurements. Since the error state dynamics is usually of lower dimension than the system state and propagate slower, the estimation is more robust than EKF and the computational complexity of the filter can also be reduced.
    
    The following subsections define the ESKF model to be used in this thesis for drone localization. It is based on the model defined in Joan Sola's work \cite{sola_quaternion_2017} in chapter 5 and 6, with some small modifications.

\subsection{States in ESKF}

    ESKF consist of 3 sets of states, namely \textbf{true-}, \textbf{nominal-} and \textbf{error-state}.
    
    \textbf{Nominal state} is the predicted state considering the high frequency measurements without taking the noise into account. In case of this thesis, the nominal state is predicted by dead-reckoning the IMU data.
    
    \textbf{Error-state} refers to the difference between the predicted state of the system and the true state of the system.
    
    \textbf{True state} refers to the composition of the nominal state and error state. This is the system state that is desired eventually.
    
    The true states to be estimated are:

    \begin{itemize}
        \item Position in $\text{m}$ in NED frame: $\mathbf{p_t} = \begin{bmatrix} p_n & p_e & p_d\end{bmatrix}^\top$
        
        \item Velocity in $\text{ms}^{-1}$ in NED frame: $\mathbf{v_t} = \begin{bmatrix} v_n & v_e & v_d\end{bmatrix}^\top$
        
        \item Attitude in quanternion in body frame: $\mathbf{q_t} = \begin{bmatrix} q_w & q_x & q_y & q_z\end{bmatrix}^\top$

        \item Accelerometer bias in $\text{ms}^{-2}$: $\mathbf{a_{tb}} = \begin{bmatrix} a_{bx} & a_{by} & a_{bz}\end{bmatrix}^\top$

        \item Gyroscope bias in $\text{rad}^{-1}$: $\boldsymbol{\omega_{\mathbf{\text{tb}}}} = \begin{bmatrix} \omega_{bx} & \omega_{by} & \omega_{bz}\end{bmatrix}^\top$
    \end{itemize}
    
    And the nominal state vector is similar to the true state vector:
    
    \begin{equation}
        \mathbf{x} = \begin{bmatrix} \mathbf{p} \\ \mathbf{v} \\ \mathbf{q} \\ \mathbf{a}_{b} \\ \boldsymbol{\omega}_{b}
        \end{bmatrix}
    \end{equation}
    
    For the error-state, the angle vector instead of quaternion is used for attitude error as the dynamics of it is simpler and more intuitive:

    $$
    \boldsymbol{\theta} = \begin{bmatrix} \theta_{x} & \theta_{y} & \theta_{z}\end{bmatrix}^\top
    $$
    
    \begin{equation}
        \mathbf{\delta x} = \begin{bmatrix} \delta \boldsymbol{p} \\
        \delta \mathbf{v} \\
        \delta \boldsymbol{\theta} \\
        \delta \mathbf{a}_{b} \\
        \delta \boldsymbol{\omega}_{b}
        \end{bmatrix}
    \end{equation}
    
    The true state vector is then the composition of both:
    
    \begin{equation}
        \mathbf{x}_t = \begin{bmatrix} \mathbf{p}_t \\ \mathbf{v}_t \\ \mathbf{q}_t \\ \mathbf{a}_{bt} \\ \boldsymbol{\omega}_{bt}
        \end{bmatrix} = \begin{bmatrix}
        \mathbf{p} + \delta \mathbf{p} \\
        \mathbf{v} + \delta \mathbf{v} \\
        \mathbf{q} \otimes \delta \mathbf{q} \\
        \mathbf{a}_b + \delta \mathbf{a}_b \\
        \boldsymbol{\omega}_b + \delta \boldsymbol{\omega}_b
        \end{bmatrix}
    \end{equation}

    where the symbol $\otimes$ represents quaternion multiplication.

\subsection{IMU Measurements}

    The IMU provides acceleration and angular rate in sensor frame. They're aligned to body frame before putting into the filter.

    \begin{itemize}
        \item Acceleration in body frame: $\mathbf{a}_m = \begin{bmatrix} a_{mf} & a_{mr} & a_{md} \end{bmatrix}^\top$

        \item Angular rate in body frame: $\boldsymbol{\omega}_m = \begin{bmatrix} \omega_{mr} & \omega_{mp} & \omega_{my} \end{bmatrix}^\top$
    \end{itemize}
    
    where $m$ stands for measurement, notations $f$, $r$, $d$, $r$, $p$, $y$ stand for front, right, down, roll, pitch, and yaw respectively.
    
    The true- and nominal- state acceleration and attitude are associated with those measurements:
    
    \begin{align}
        &\mathbf{a}_t = \mathbf{R}_t (\mathbf{a}_m - \mathbf{a}_{bt} - \mathbf{a}_n) + \mathbf{g}_t \\
        &\mathbf{a} = \mathbf{R}_t (\mathbf{a}_m - \mathbf{a}_{bt}) + \mathbf{g}_t \\
        &\boldsymbol{\omega}_t = \boldsymbol{\omega}_m - \boldsymbol{\omega}_{bt} - \boldsymbol{\omega}_n \\
        &\boldsymbol{\omega} = \boldsymbol{\omega}_m - \boldsymbol{\omega}_{bt}
    \end{align}

    where $R$ is the rotation matrix from body frame to NED frame. To avoid gimbal lock, quaternions of the attitude are used instead of Euler angles. The form of $\mathbf{R}$ is a direction cosine matrix, which is a rotation matrix in homogeneous form:

    \begin{align}
        \mathbf{R}= \begin{bmatrix}
        q_{w}^{2}+q_{x}^{2}-q_{y}^{2}-q_{z}^{2} & 2(q_{x}q_{y}-q_{w}q_{z})&2(q_{w}q_{y}+q_{x}q_{z})\\
        2(q_{x}q_{y}+q_{w}q_{z})&q_{w}^{2}-q_{x}^{2}+q_{y}^{2}-q_{z}^{2}&2(q_{y}q_{z}-q_{w}q_{x})\\
        2(q_{x}q_{z}-q_{w}q_{y})&2(q_{w}q_{x}+q_{y}q_{z})&q_{w}^{2}-q_{x}^{2}-q_{y}^{2}+q_{z}^{2}
        \end{bmatrix}
    \end{align}

\subsection{Nominal State Transition}
\label{Chapter::ESKF::Nominal_State_Transition}

    The nominal state transition is part of the prediction step, in which the current system state is predicted  according to the transition model without considering any error or noise in the measurement. The new system state will be used as a starting point in error state estimation.
    
    The transition in continuous time is defined as:

    \begin{align}
        &\dot{\mathbf{p}} = \mathbf{v} \\
        &\dot{\mathbf{v}} = \mathbf{R}(\mathbf{a}_m - \mathbf{a}_b) + \mathbf{g} \\
        &\dot{\mathbf{q}} = \frac{1}{2} \mathbf{q} \otimes (\boldsymbol{\omega}_m - \boldsymbol{\omega}_b) \\
        &\dot{\mathbf{a}}_b = 0 \\
        &\dot{\boldsymbol{\omega}}_b = 0
    \end{align}
    
    where $\mathbf{g}$ is the gravity vector which is fixed as $\begin{bmatrix} 0, 0, 9.80665 \end{bmatrix}^\top$.
    
    The nominal acceleration $\dot{\mathbf{v}}$ can be regarded as the accelerometer readings compensated with the estimated accelerometer bias, rotated from sensor frame to NED frame, and without gravity factor.
    
    Note that the biases won't change in nominal state transition, as there is no relevant information from the IMU measurements. In spite of this, these biases are important as they would be used to correct the accelerations and angular velocities in nominal state prediction step in order to provide more accurate state estimation in between GNSS measurement updates.
    
    In actual implementation, the transition is in discrete time. The corresponding transition model is then defined as:
    
    \begin{align}
        &\mathbf{p} \gets \mathbf{p} + \mathbf{v} \cdot \varDelta t + \frac{1}{2} (\mathbf{R}(\mathbf{a}_m - \mathbf{a}_b) + \mathbf{g}) \varDelta t^2 \\
        &\mathbf{v} \gets \mathbf{v} + (\mathbf{R}(\mathbf{a}_m - \mathbf{a}_b) + \mathbf{g}) \varDelta t \\
        &\mathbf{q} \gets \mathbf{q} \otimes \mathbf{q}\{(\boldsymbol{\omega}_m - \boldsymbol{\omega}_b) \varDelta t\} \label{eq::nominal_att_update} \\
        &\mathbf{a}_b \gets \mathbf{a}_b \\
        &\boldsymbol{\omega}_b \gets \boldsymbol{\omega}_b
    \end{align}

    $x \ ← f (x, •)$ means time update, which is equivalent to $x_k = f(x_{k-1}, •)$.
    
    The symbol $\otimes$ in equation \eqref{eq::nominal_att_update} means quaternion multiplication. $q\{(\omega_m - \omega_b) \varDelta t\}$ is the quanternion associated to the rotation within the time interval. This quanternion is deduced from the angular velocities in row $\phi$, pitch $\theta$, yaw $\psi$:

    \begin{equation}
        \mathbf{q}\{(\boldsymbol{\omega}_m - \boldsymbol{\omega}_b) \varDelta t\} =\begin{bmatrix}
        1 \\ 
        (\omega_\phi - \omega_{b\phi}) \cdot \varDelta t / 2  \\
        (\omega_\theta - \omega_{b\theta}) \cdot \varDelta t / 2 \\
        (\omega_\psi - \omega_{b\psi}) \cdot \varDelta t / 2
        \end{bmatrix}
    \end{equation}

\subsection{Error State Transition}
\label{Chapter::ESKF::Error_State_Transition}

    The error state transition in continuous time is:
    
    \begin{align}
        &\dot{\delta \mathbf{p}} = \delta \mathbf{v} \\
        &\dot{\delta \mathbf{v}} = -\mathbf{R}[\mathbf{a}_m - \mathbf{a}_b]_\times \delta \theta - \mathbf{R} \delta \mathbf{a}_b - \mathbf{R} \mathbf{a}_n \\
        &\dot{\delta \boldsymbol{\theta}} = -[\boldsymbol{\omega}_m - \boldsymbol{\omega}_b]_\times \delta \boldsymbol{\theta} - \delta \boldsymbol{\omega}_b - \boldsymbol{\omega}_n\\
        &\dot{\delta \mathbf{a}}_b = \mathbf{a}_w \\
        &\dot{\delta \boldsymbol{\omega}}_b = \boldsymbol{\omega}_w
    \end{align}
    
    where $\mathbf{a}_w$ and $\boldsymbol{\omega}_w$ are just zero-mean Gaussian noise of accelerometer and gyroscope respectively.
    
    $[\mathbf{a}_m - \mathbf{a}_b]_\times$ and $[\boldsymbol{\omega}_m - \boldsymbol{\omega}_b]_\times$ are the acceleration and angular rate compensated with the estimated biases. The subscript $_\times$ indicates they're in the form of skew matrix.
    
    The derivation of $\dot{\delta \mathbf{v}}$ and $\dot{\delta \boldsymbol{\theta}}$ are available in section 5.3.3 in \cite{sola_quaternion_2017}.
    
    In discrete time:
    
    \begin{align}
        &\delta \mathbf{p} \gets \delta \mathbf{p} + \delta \mathbf{v} \varDelta t\\
        &\delta \mathbf{v} \gets \delta \mathbf{v} + (-\mathbf{R} [\mathbf{a}_m - \mathbf{a}_b]_\times \delta \boldsymbol{\theta}) - \mathbf{R} \delta \mathbf{a}_b) \varDelta t + \mathbf{v}_i\\
        &\delta \boldsymbol{\theta} \gets \mathbf{R}^\top \{(\boldsymbol{\omega}_m - \boldsymbol{\omega}_b) \varDelta t\} \delta \boldsymbol{\theta} - \delta \boldsymbol{\omega}_b \varDelta t + \boldsymbol{\theta}_i \\
        &\delta \mathbf{a}_b \gets \delta \mathbf{a}_b + \mathbf{a}_i \\
        &\delta \boldsymbol{\omega}_b \gets \delta \boldsymbol{\omega}_b + \boldsymbol{\omega}_i
    \end{align}
    
    where $\mathbf{v}_i$, $\boldsymbol{\theta}_i$, $\mathbf{a}_i$, and $\boldsymbol{\omega}_i$ are zero-mean Gaussian noise

    The corresponding error-state transition matrix is:
    
    \begin{equation}
        \mathbf{F_x} = \frac{\partial f}{\partial \delta \mathbf{x}} |_{\mathbf{x}, \mathbf{u}_m} = \begin{bmatrix}
        \mathbf{I} & \mathbf{I} \varDelta t& 0 & 0 & 0 \\
        0 & \mathbf{I} & -\mathbf{R} \cdot [\mathbf{a}_m - \mathbf{a}_b]_\times \varDelta t & -\mathbf{R} \varDelta t & 0 \\
        0 & 0 & \mathbf{R}^\top[\boldsymbol{\omega}_m - \boldsymbol{\omega}_b]_\times \varDelta t & 0 & -\mathbf{I} \varDelta t \\
        0 & 0 & 0 & \mathbf{I} & 0\\
        0 & 0 & 0 & 0 & \mathbf{I}
        \end{bmatrix}
    \end{equation}

\subsection{Initialization}

    It is assumed that the drone is static at a certain position and attitude at the beginning of each flight. Since IMU doesn't provide an absolute position, the initial position is therefore set at the initial stable position in NED frame from GNSS measurement before the start of movement. On the other hand, the initial attitude is computed from the initial accelerometer and magnetometer readings.

\subsection{Nominal State Prediction}


    The nominal state at time step $k+1$ is predicted with the discrete time nominal state transition:
    
    \begin{equation}
        \mathbf{x}_{k+1} = \mathbf{A}\mathbf{x}_k
    \end{equation}
    
    In this equation, the matrix $\mathbf{A}$ follows the transition model  as described in section \ref{Chapter::ESKF::Nominal_State_Transition}. In the program code implemented, each state parameter is updated individually. Therefore, the matrix $\mathbf{A}$ has not been explicitly defined.

\subsection{Error state prediction}

    The prediction of the error state is defined as:
    
    \begin{equation}
        \hat{\delta \mathbf{x}} \gets \mathbf{F_x} (\mathbf{x}, \mathbf{u}_m) \cdot \hat{\delta \mathbf{x}}
    \end{equation}
    
    Since the mean of errors is initialized as 0 (i.e.: $\delta \mathbf{x} = [0])$, the predicted error state would always be 0. Because of this, the error state prediction does not need to be handled.
    
    For the covariance, it will be updated as standard Kalman Filter does:
    
    \begin{equation}
        \mathbf{P} \gets \mathbf{F_x} \mathbf{P} \mathbf{F_x}^\top + \mathbf{F_i} \mathbf{Q_i} \mathbf{F_i}^\top
    \end{equation}
    
    The variable $\mathbf{F_x}$ is the error-state transition matrix described in section \ref{Chapter::ESKF::Error_State_Transition}. $\mathbf{F_i Q F_i^\top}$ is the process noise, where:
    
    \begin{equation}
        \mathbf{F_i} = \frac{\partial f}{\partial \mathbf{i}} |_{\mathbf{x}, \mathbf{u}_m} = \begin{bmatrix}
        0 & 0 & 0 & 0 \\
        \mathbf{I} & 0 & 0 & 0 \\
        0 & \mathbf{I} & 0 & 0 \\
        0 & 0 & \mathbf{I} & 0 \\
        0 & 0 & 0 & \mathbf{I} \\
        \end{bmatrix}
    \end{equation}
    
    \begin{equation}
        \mathbf{Q_i} = \begin{bmatrix}
        \mathbf{V_i} & 0 & 0 & 0 \\
        0 & \boldsymbol{\Theta}_\mathbf{i} & 0 & 0 \\
         0 & 0 & \mathbf{A_i} & 0 \\
        0 & 0 & 0 & \boldsymbol{\Omega}_\mathbf{i} \\
        \end{bmatrix}
        =
        \begin{bmatrix}
        \sigma_v^2 \varDelta t^2 \mathbf{I} & 0 & 0 & 0 \\
        0 & \sigma_\theta^2 \varDelta t^2 \mathbf{I} & 0 & 0 \\
         0 & 0 & \sigma_{a_b}^2 \varDelta t^2 \mathbf{I} & 0 \\
        0 & 0 & 0 & \sigma_{\omega_b}^2 \varDelta t^2 \mathbf{I} \\
        \end{bmatrix}
    \end{equation}

    and $\mathbf{V_i}$, $\boldsymbol{\Theta}_\mathbf{i}$, $\mathbf{A_i}$ and $\boldsymbol{\Omega}_\mathbf{i}$ are the perturbations impulses in velocity, attitude and accelerometer and gyroscope biases. A justification can be found in Appendix E of Joan Sola's work \cite{sola_quaternion_2017}.
    
    As $\mathbf{F_i Q F_i^\top}$ is not zeroes, $\mathbf{P}$ would growth over time. Also, since the covariance for attitude uses Euler angles instead of quaternions. The shape of $\mathbf{P}$ is 15x15.

\subsection{GNSS Measurement Update}

    GNSS can provide the position in LLA, which is able to convert to position in NED frame. and derive into velocity.
    
    \begin{equation}
        \mathbf{y} = \begin{bmatrix}\mathbf{p}_{m} \\ \mathbf{v}_{m}\end{bmatrix} =
        \begin{bmatrix}\mathbf{p}_{m} \\ (\mathbf{v}_{m_{k}} - \mathbf{v}_{m_{k-1}})  \varDelta t\end{bmatrix}
    \end{equation}
    
    The measurement $y$ from GNSS depends on the system state, such that:
    
    \begin{equation}
        \mathbf{y} = h(\mathbf{x}_t) + v
    \end{equation}
    
    where $h(\mathbf{x}_t)$ is the state-to-measurement function and $v$ is the measurement noise
    
    The update steps are similar to the standard KF:
    
    \begin{align}
        &\mathbf{K} = \mathbf{PH}^\top (\mathbf{HPH}^\top + \mathbf{V})^{-1} \\
        &\hat{\delta \mathbf{x}} \gets \mathbf{K}(\mathbf{y} - h(\hat{\mathbf{x}_t})) \\
        &\mathbf{P} \gets (\mathbf{I} - \mathbf{KH}) \mathbf{P} (\mathbf{I} - \mathbf{KH})^\top + \mathbf{KVK}^\top
    \end{align}
    
    The state that is being estimated is the error-state, so $\mathbf{y} - h(\hat{\mathbf{x}_t})$ is the difference between the measurement and the previously estimated state.
    
\subsection{Injection}
    
    Since the true-state is the composition of the nominal- and error-state, it can be retrieved by injecting the error state estimated into the predicted nominal state:
    
    \begin{equation}
        \mathbf{x} \gets \mathbf{x} \oplus \hat{\delta \mathbf{x}}
    \end{equation}
    
    which is equivalent to:
    
    \begin{equation}
        \hat{\mathbf{x}} = \begin{bmatrix}
        \mathbf{p} + \hat{\delta \mathbf{p}} \\
        \mathbf{v} + \hat{\delta \mathbf{v}} \\
        \mathbf{q} \otimes \hat{\delta \mathbf{q}} \\
        \mathbf{a}_b + \hat{\delta \mathbf{a}_b} \\
        \boldsymbol{\omega}_b + \hat{\delta \boldsymbol{\omega}_b}
        \end{bmatrix}
    \end{equation}

\subsection{ESKF Reset}

    During ESKF reset, the estimated error-state will be discarded while the covariance of the error has to be updated accordingly:
    
    \begin{align}
        &\delta \mathbf{x} \gets 0 \\
        &\mathbf{P} \gets \mathbf{GPG}^\top
    \end{align}
    
    and the matrix $G$ is defined as:
    
    \begin{equation}
        \mathbf{G} = \begin{bmatrix}
        \mathbf{I}_6 & 0 & 0 \\
        0 & \mathbf{I} - [\frac{1}{2} \hat{\delta \boldsymbol{\theta}}] \\
        0 & 0 &\mathbf{I}_6
        \end{bmatrix}
    \end{equation}
    
    It means most of the covariance terms remind unchanged except attitude error.