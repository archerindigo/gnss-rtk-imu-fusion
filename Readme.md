# Implementation and Evaluation of a GNSS-RTK Based Drone Positioning System and  IMU Sensor Fusion through Error-State Kalman Filtering

## Abstract

As drones become more popular in industrial applications, there is a need to establish precise real-time positioning. The real-time kinematic of global positioning satellite system (GNSS-RTK), as an advancement of the ordinary GNSS, is emerging due to its theoretical centimeter-level precision and accuracy, as well as the reduced cost in establishing the system. Additionally, the GNSS-RTK may provide better reference to set up ground truth positions in the evaluation of other positioning methods.

This thesis performed three types of experiment to investigate the actual performance of GNSS-RTK in field environment, including real drone flying. A fusion between GNSS-RTK and a low-cost inertial measurement unit (IMU) using the Error-state Kalman Filter has also been implemented to investigate whether such fusion can achieve more reliable positioning by the mean of providing reasonable estimations when GNSS is temporarily unavailable.

The experiment results show that GNSS-RTK is feasible to be used independently to provide accurate positional information as it claims. For the GNSS-IMU fusion, although the ESKF implemented was able to compensate part of the errors from the IMU measurements, the degree of noise and biases in the IMU used was simply too high. Therefore, the fusion relied heavily on the GNSS measurements, and the IMU measurements alone failed to provide reasonable estimations. The issues encountered are being recorded and analyzed.

## Directory

Photo: Some photos and images to be used for readme.

Programs: Contains the programs designed for the thesis.

Test_Data: Field test data and the analysis, figures.

Presentation: Slides used in the presentations.

Thesis: Contains the final pdf version of the thesis and the LaTeX source.

## Measurement Platform Setup

The measurement platform is to be mounted on a mobile vehicle and for collecting synchronized data from the GNSS module and the IMU module. The full connection of the measurement platform is as follows:

![](Images/measurement_platform.png)

The headers for the ZEN-F9P are just for holding the breakout board in place. There is no connection.

For the IMU, the NC pin is exposed next to the shorter header. By this one can easily connect the IMU in the correct orientation.

Below is the block diagram of the platform:

![](Images/block_diag.png)

Mounting of the platform on a drone:

![](Images/drone_top.png)

![](Images/drone_platform.png)
=======

## Software

All the programs are stored in the directory `Programs`, and `rtk_fusion.py` is the script to be run on the Raspberry Pi. Please refer to the readme in `Programs` for further information.
