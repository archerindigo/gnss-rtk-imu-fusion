"""
Extended Kalman Filter for navigation

States to estimate:
- Position in NED frame
- Velocity in NED frame
- Attitude in quaternion
- Accelerometer bias
- Gyroscope bias

Input IMU data assumed to be aligned with NED frame
"""

import numpy as np
import pymap3d as pm
from utils import orientation
from scipy.spatial.transform import Rotation as R


class EKF_ins_gnss:

    gv = np.array([0, 0, 9.80665])      # gravity vector

    def __init__(self,
                 ref_lla: np.array,
                 pos_ned0,
                 att_q0,
                 sigma_pos,
                 sigma_vel,
                 sigma_acc,
                 sigma_gyr,
                 sigma_abias,
                 sigma_gbias):

        self.ref_lla = ref_lla[0], ref_lla[1], ref_lla[2]   # reference point to set-up NED frame

        # states
        self.pos = pos_ned0               # position: initialized by user
        self.vel = np.zeros(3)         # velocity: assume static
        self.att_q = att_q0             # attitude: initially should be resolved from initial IMU data
        self.att_rpy = orientation.quat2rpy(att_q0)
        self.acc_bias = np.zeros(3)    # no known bias for acc at the beginning
        self.gyro_bias = np.zeros(3)   # no known bias for gyro at the beginning

        # Initialize covariance
        self.P = np.zeros((15, 15))
        #self.P[0:3, 0:3] = sigma_pos**2 * np.eye(3)
        #self.P[3:6, 3:6] = sigma_vel**2 * np.eye(3)
        #self.P[6:9, 6:9] = sigma_gyr**2 * np.eye(3)    # error in rotation angles
        #self.P[9:12, 9:12] = sigma_abias**2 * np.eye(3)
        #self.P[12:15, 12:15] = sigma_gbias**2 * np.eye(3)
        #print(f"P0 = {self.P}")

        # initialize noise covariance
        self.Q_err = np.zeros((12, 12))
        self.Q_err[0:3, 0:3] = sigma_acc**2 * np.eye(3)
        self.Q_err[3:6, 3:6] = sigma_gyr**2 * np.eye(3)
        self.Q_err[6:9, 6:9] = sigma_abias**2 * np.eye(3)
        self.Q_err[9:12, 9:12] = sigma_gbias**2 * np.eye(3)
        #print(f"Q0 = {self.Q_err}")

    def update_attitude_rpy(self, g, dt):
        """
        Update attitude in rpy angles
        """

        for i, att in enumerate(self.att_rpy):
            new_angle = att + g[i] * dt
            if new_angle > np.pi:
                new_angle -= np.pi * 2
            elif new_angle < -np.pi:
                new_angle += np.pi * 2
            att = new_angle
            self.att_rpy[i] = new_angle
        #print(f"{self.att_rpy}")

    def update_attitude_quat(self, g, dt):
        """
        Update attitude in quaternion
        """

        dq = np.array([1, 0.5 * g[0] * dt, 0.5 * g[1] * dt, 0.5 * g[2] * dt])
        q = orientation.quatmul(self.att_q, dq)
        q /= np.linalg.norm(q)    # normalize
        if q[0] < 0:     # flip if negative
            q *= -1
        self.att_q = q

    def injection(self, delta_x):
        """
        Update the states by the calculated error state

        args:
            delta_x: error state deviation. Shape (15,)
        """
        # debug: print delta_x
        #print(f"delta_v={delta_x[3:6]}")
        #print(f"delta_att={delta_x[6:9]}")


        self.pos += delta_x[0:3]
        self.vel += delta_x[3:6]
        self.acc_bias += delta_x[9:12]
        self.gyro_bias += delta_x[12:15]

        # inject attitude
        #print(f"Att b4 update: {orientation.quat2rpy(self.att_q, True)}")
        self.update_attitude_quat(delta_x[6:9], 1)  # update the attitude by the error in roll, pitch, yaw angle
                                                    # dt set to 1 as the update is instant
        #print(f"Att aft update: {orientation.quat2rpy(self.att_q, True)}")

        #print(f"acc_bias = {self.acc_bias} gyro_bias = {self.gyro_bias}")

    def update_imu(self, acc, gyro, dt):
        """
        Perform the prediction step with the data from IMU

        args:
            acc (np.ndarray): acceleration values in m/s^-2 in body frame
            gyro (np.ndarray): angular rate in rad/s in body frame
            dt: time step
        """

        # correct bias
        acc -= self.acc_bias
        gyro -= self.gyro_bias

        # update attitude
        self.update_attitude_quat(gyro, dt) # update by quaternion
        self.update_attitude_rpy(gyro, dt)  # update attitude by rpy

        R_n2b = orientation.quat2dcm(self.att_q)     # DCM
        #R_n2b = orientation.quat2R(self.att_q)     # rotation matrix
        #print(f"attitude from quat: {orientation.quat2rpy(self.att_q, True)} q= {self.att_q}")
        #print(f"attitude from rpy: {np.rad2deg(self.att_rpy)} q= {self.att_q}")
        #print(f"R by my func: {R_n2b}")
        #R_n2b = orientation.quat2dcm(orientation.rpy2quat(self.att_rpy))     # Test: use euler angles to find R
        #q = [self.att_q[1], self.att_q[2], self.att_q[3], self.att_q[0]]
        #r = R.from_quat(q)     # Test: use scipy to find R
        #r = R.from_euler("xyz", self.att_rpy)
        #R_n2b = R.as_matrix(r)
        R_b2n = R_n2b.T     # rotation matrix from body frame to navigation frame



        # resolve acc in NED
        self.acc_ned = acc_ned = np.matmul(R_b2n, acc) + self.gv   # get acc. in nav frame and cancel out gravity

        #acc_bias_ned = [-0.135, 0.2, 0]     # Test: additional bias
        #self.acc_ned -= acc_bias_ned

        # Test: high pass filter
        #acc_mag = np.linalg.norm(self.acc_ned)
        #print(f"acc_mag: {acc_mag}")
        #if np.linalg.norm(self.acc_ned) < 0.5:
        #    self.acc_ned = acc_ned = np.zeros(3)
        #print(f"att: {orientation.quat2rpy(self.att_q, True)}, acc_b4: {acc} acc_aft: {acc_ned}")

        # update position and velocity
        self.pos += self.vel * dt + 0.5 * acc_ned * dt * dt
        self.vel += acc_ned * dt

        # Test: Use acc. magnitude and orientation to resolve pos, vel
        #acc_2d_mag = np.linalg.norm(acc_ned[0:2])
        #a_x = acc_2d_mag * np.cos(self.att_rpy[2])
        #a_y = acc_2d_mag * np.sin(self.att_rpy[2])
        #a_z = acc_ned[2]
        #vel_2d_mag = np.linalg.norm(self.vel[0:2])
        #v_x = vel_2d_mag * np.cos(self.att_rpy[2])
        #v_y = vel_2d_mag * np.sin(self.att_rpy[2])
        #v_z = self.vel[2]

        #v_z = np.linalg.norm([self.vel[0], self.vel[2]]) * np.cos(- self.att_rpy[1])
        #print(f"v = {v_x}, {v_y}, {v_z}")
        #self.pos = np.array([self.pos[0] + self.vel[0] * dt + 0.5 * a_x * dt * dt,
        #                     self.pos[1] + self.vel[1] * dt + 0.5 * a_y * dt * dt,
        #                     self.pos[2] + self.vel[2] * dt + 0.5 * a_z * dt * dt])
        #self.pos = np.array([self.pos[0] + v_x * dt,
        #                     self.pos[1] + v_y * dt,
                             #self.pos[2]] + v_z * dt)
        #                     self.pos[2]])
        #self.vel = np.array([self.vel[0] + a_x * dt, self.vel[1] + a_y * dt, self.vel[2]])
        #print(f"acc_2d_mag = {acc_2d_mag} acc_ned = {acc_ned} yaw={np.rad2deg(self.att_rpy[2])} vel = {self.vel}, a_x = {a_x *dt *dt *0.5} a_y = {a_y * dt * dt * 0.5}")

        # Biases remain unchanged during prediction, so they're not handled

        # Now the instance contains the predicted nominal state
        # Time to predict the error state covariance

        f_x = np.eye(15)    # system transition Jacobians matrix
        f_x[0:3, 3:6] = np.eye(3) * dt
        f_x[3:6, 6:9] = -R_b2n * orientation.skew_mat(self.acc_ned) * dt
        f_x[3:6, 9:12] = -R_b2n * dt
        f_x[6:9, 6:9] = R_n2b * orientation.skew_mat(gyro) * dt
        f_x[6:9, 12:15] = -np.eye(3) * dt

        f_i = np.zeros((15,12))
        f_i[3:6, 0:3] = -R_b2n
        f_i[6:9, 3:6] = -np.eye(3)
        f_i[9:15, 6:12] = np.eye(6)

        Q = self.Q_err * dt        # use it if noise is proportional to time interval (maybe *dt**2 ?)
        #Q = self.Q_err
        self.P = f_x @ self.P @ f_x.T + f_i @ Q @ f_i.T   # predicted covariance

    def update_gnss(self, lla,
                    prev_lla,
                    sigma_hpos,
                    sigma_vpos,
                    sigma_hvel,
                    sigma_vvel,
                    dt=1.0):
        """
        Update states with gnss data

        Args:
            lla: GNSS position in LLA. Shape (3,1)
            prev_lla: previous LLA position from GNSS
            sigma_*: sigma values for horizonal and vertical position/ velocity
            dt: time interval between GNSS update
        """
        gnss_ned = pm.geodetic2ned(lla[0], lla[1], lla[2],
                              self.ref_lla[0], self.ref_lla[1], self.ref_lla[2])

        gnss_ned_prev = pm.geodetic2ned(prev_lla[0], prev_lla[1], prev_lla[2],
                              self.ref_lla[0], self.ref_lla[1], self.ref_lla[2])

        vel_ned = (np.array(gnss_ned) - np.array(gnss_ned_prev)) / dt    # derive velocity from GNSS position only
        #vel_ned = (np.array(gnss_ned) - self.pos) / dt    # derive velocity from GNSS position and estimated position

        # handle covariance of GNSS
        gnss_cov = np.eye(6)
        #gnss_cov = np.eye(3)    # pos only
        gnss_cov[0:2, 0:2] = np.eye(2) * sigma_hpos ** 2
        gnss_cov[2, 2] = sigma_vpos ** 2
        gnss_cov[3:5, 3:5] = np.eye(2) * sigma_hvel ** 2
        gnss_cov[5,5] = sigma_vvel ** 2
        #print(f"Measurement Cov:\n{gnss_cov}")

        # Update error-state
        H = np.block([np.eye(6), np.zeros((6,9))])
        #H = np.block([np.eye(3), np.zeros((3,12))])     # test: pos only
        pos_diff = gnss_ned - self.pos
        vel_diff = vel_ned - self.vel
        y_diff = np.block([pos_diff, vel_diff])
        #y_diff = np.block([pos_diff])   # test: pos only
        S = H @ self.P @ H.T + gnss_cov     # HPH^T + V ; Shape(6,6)
        #print(f"y_diff = {y_diff}\nS ({S.shape})=\n{S}")

        K = self.P @ H.T @ np.linalg.inv(S) # Kalman gain. Shape (15,6)
        dx_diff = K @ y_diff    # FIXME: gyro bias get very large

        # Update covariance
        I = np.eye(15)
        Jo = I - K @ H
        #self.P = (np.eye(15) - K @ H) @ self.P     # the simpliest but instable form
        self.P = Jo @ self.P @ Jo.T + K @ gnss_cov @ K.T  # Joseph form

        self.injection(dx_diff)


        # ESKF reset
        # No further action is required on error state as it is not preserved
        # P needs to be updated after injection (P <- GPG^T)
        G = np.eye(15)
        G[6:9, 6:9] = np.eye(3) - orientation.skew_mat(dx_diff[6:9])
        self.P = G @ self.P @ G.T