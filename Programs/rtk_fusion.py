import argparse
import csv
from datetime import datetime
from io import BufferedReader
import logging
import numpy as np
from queue import Queue, Empty
import qwiic_icm20948
from serial import Serial
import sys
from time import sleep, strftime, time
from threading import Thread, Lock, Event
from pyubx2 import (
    UBXReader,
    NMEA_PROTOCOL,
    UBX_PROTOCOL,
    RTCM3_PROTOCOL,
    protocol,
)
from pygnssutils import GNSSNTRIPClient, VERBOSITY_LOW, VERBOSITY_MEDIUM, haversine
from pyrtcm import RTCM_MSGIDS
import yaml

######## Constants #########
ACCL_RANGE_2G = 0x00
ACCL_RANGE_4G = 0x01
ACCL_RANGE_8G = 0x02
ACCL_RANGE_16G = 0x03
GYRO_RANGE_250DPS = 0x00
GYRO_RANGE_500DPS = 0x01
GYRO_RANGE_1000DPS = 0x02
GYRO_RANGE_2000DPS = 0x03

GA = 9.80665    # gravitational acceleration
DEG2RAD = np.pi / 180
RAD2DEG = 180 / np.pi

######## Parameters ##########

# Config from yaml file

_start_date = datetime.now().strftime("%Y%m%d_%H_%M_%S")
_cfg = {
    'test_info': {
        'test_name': None,
        'test_code': None,
        'log_dir': "log"
#        'sensor_log': f"log/sensor_data_{_start_date}.csv",
#        'gnss_log': f"log/gnss_message_{_start_date}.ubx"
    },

    'gnss': {
        'serial_port': '/dev/ttyACM0',
        'baudrate': 460800,
        'timeout': 3,
        'msl2hae': 0.0
    },

    'ntrip': {
        'enable': True,
        'address': None,
        'port': 2101,
        'mount_point': None,
        'username': None,
        'password': None,
        'gga_mode': 1,
        'gga_interval': 1,
        'reflat': 0.0,
        'reflon': 0.0,
        'refalt': 0.0,
        'refsep': 0.0
    },

    'imu': {
            'acc_bias': [0, 0, 0],
            'gyr_bias': [0, 0, 0],
            'mag_bias': [0, 0, 0]
    },

    'google_map': {
        'api_key': None,
        'bias': [3.6e-06, 5.5e-06, 0.0]
    }
}

# Other parameters which are fixed or
# can only be modified from command line arguments at the moment

# IMU parameters
ACCL_RANGE_MODE = ACCL_RANGE_4G
ACCL_DLPF_ENABLE = False
GYRO_RANGE_MODE = GYRO_RANGE_500DPS
GYRO_DLPF_ENABLE = False

# Logging parameters
LOG_TO_FILE = False
LOGGING_FORMAT = "%(asctime)s::%(levelname)-8s %(message)s"
LOGGING_LEVEL = logging.INFO
LOG_GNSS_FULL = False   # True: Log parsed GNSS data; False: Log only the types of data in flow

_start_date = datetime.now().strftime("%Y%m%d_%H_%M_%S")
SENSOR_LOG_FILENAME = f"log/sensor_data_{_start_date}.csv"

GNSS_MSG_LOG_ENABLE = True  # True: log raw output from GNSS module to file
GNSS_MSG_LOG_FILENAME = f"log/gnss_msg_{_start_date}.ubx"

########### Global Variables ############

# Thread related
_ths_stop = Event()   # Event to stop all threads
_serial_lock = Lock()
_ntrip_queue = Queue()

# Shared IMU/GNSS results
_gnss_data_lock = Lock()
_gnss_new_data_evt = Event()

_imu_new_data_evt = Event()
_imu_new_data_log_evt = Event()

_sensors_data_lock = Lock()
_sensors_data_latest = dict.fromkeys(["syst", "gnsst", "lat", "lon", "marker", "alt_msl", "alt_hae", "quality", "hdop",\
                            "diff_age", "sv_num", "gs", "cogt", "cogm",  "gnss_new_data",\
                            "ax_raw", "ay_raw", "az_raw", "ax_ms2_cal", "ay_ms2_cal", "az_ms2_cal",\
                            "gx_raw", "gy_raw", "gz_raw", "gx_rad_cal", "gy_rad_cal", "gz_rad_cal",\
                            "mx_raw", "my_raw", "mz_raw", "mx_uT_cal", "my_uT_cal", "mz_uT_cal"])
_sensors_data_latest["marker"] = 0
_sensors_log_done = Event()
_sensors_log_done.set()

############# Functions ##################

def init_argparse():
    parser = argparse.ArgumentParser(
        description="Sensor data collection and fusion positioning"
    )

    parser.add_argument(
        "-c", "--cfg", type=str, default="config.yaml",
        help="path to the configuration file (default: config.yaml)"
    )

    parser.add_argument(
        "-n", "--no_ntrip", action="store_true",
        help="disable ntrip client"
    )

    parser.add_argument(
        "-t", "--use_datetime", action="store_true",
        help="use datetime for logs' filename instead of test code"
    )

    parser.add_argument(
        "-d", "--debug", action="store_true",
        help="set log level to debug"
    )

    return parser

def parse_config_arg():
    global LOGGING_LEVEL
    global SENSOR_LOG_FILENAME
    global GNSS_MSG_LOG_FILENAME

    parser = init_argparse()
    args = parser.parse_args()

    # load configuration file
    with open(args.cfg, "r") as file:
        cfg = yaml.safe_load(file)
        # update cfg sections one by one in order to keep missing keys
        for section in ["test_info", "gnss", "ntrip", "imu", "google_map"]:
            _cfg[section].update(cfg[section])

    # set other configs which come from command line arguments
    # they may override some settings made by the yaml config file
    if args.no_ntrip:
        _cfg["ntrip"]["enable"] = False  # override config file's setting

    LOGGING_LEVEL = logging.DEBUG if args.debug else logging.INFO

    log_dir = _cfg['test_info']['log_dir']
    if args.use_datetime:
        SENSOR_LOG_FILENAME=f"{log_dir}/sensor_data_{_start_date}.csv"
        GNSS_MSG_LOG_FILENAME = f"{log_dir}/gnss_msg_{_start_date}.ubx"
    else:
        tc = _cfg["test_info"]["test_code"]
        SENSOR_LOG_FILENAME=f"{log_dir}/sensor_data_{tc}.csv"
        GNSS_MSG_LOG_FILENAME = f"{log_dir}/gnss_msg_{tc}.ubx"



def read_gnss(stream, _serial_lock, stopevent):
    """
    THREADED
    Reads and parses incoming GNSS data from receiver.
    """

    msl2hae = _cfg["gnss"]["msl2hae"]

    ubr = UBXReader(
        #BufferedReader(stream),
        stream,
        protfilter=(NMEA_PROTOCOL | UBX_PROTOCOL | RTCM3_PROTOCOL),
    )

    gnss_msg_log = open(GNSS_MSG_LOG_FILENAME, 'wb')
    logging.info(f"GNSS log will be saved to: {GNSS_MSG_LOG_FILENAME}")

    while not stopevent.is_set():
        try:
            if stream.in_waiting:
                _serial_lock.acquire()
                (raw_data, parsed_data) = ubr.read()
                _serial_lock.release()
                if parsed_data:
                    idy = parsed_data.identity

                    # if it's an RXM-RTCM message, show which RTCM3 message
                    # it's acknowledging and whether it's been used or not.""
                    if idy == "RXM-RTCM":
                        nty = (
                            f" - {parsed_data.msgType} "
                            f"{'Used' if parsed_data.msgUsed > 0 else 'Not used'}"
                        )
                    else:
                        nty = ""

                    # NMEA messages
                    if idy in ["GNGGA", "GPGGA"]:
                        gnss_time = parsed_data.time
                        lat = parsed_data.lat
                        lon = parsed_data.lon
                        alt = parsed_data.alt
                        quality = parsed_data.quality
                        sv = parsed_data.numSV
                        hdop = parsed_data.HDOP
                        diff_age = parsed_data.diffAge
                        logging.info(f"GPST: {gnss_time} LLA: ({lat}, {lon}, {alt}) q={quality} sv={sv}\n")

                        _sensors_data_lock.acquire()
                        _sensors_data_latest["gnsst"] = gnss_time
                        _sensors_data_latest["lat"] = lat
                        _sensors_data_latest["lon"] = lon
                        _sensors_data_latest["alt_msl"] = alt
                        if alt != "":
                            _sensors_data_latest["alt_hae"] = float(alt) + msl2hae
                        _sensors_data_latest["quality"] = quality
                        _sensors_data_latest["sv_num"] = sv
                        _sensors_data_latest["hdop"] = hdop
                        _sensors_data_latest["diff_age"] = diff_age
                        _sensors_data_latest["gnss_new_data"] = 1   # GNGGA always come later than GNVTG so a new row of data should be marked at this point
                        _sensors_data_lock.release()
                    elif idy in ["GNVTG", "GPVTG"]:
                        _sensors_data_lock.acquire()
                        _sensors_data_latest["cogt"] = parsed_data.cogt
                        _sensors_data_latest["cogm"] = parsed_data.cogm
                        _sensors_data_latest["gs"] = parsed_data.sogk
                        _sensors_data_lock.release()
 

                    if LOG_GNSS_FULL:
                        logging.debug(parsed_data)
                    else:
                        logging.debug(f"GNSS>> {idy}{nty}")

                    if GNSS_MSG_LOG_ENABLE:
                        gnss_msg_log.write(raw_data)
        except Exception as err:
            logging.error(f"Something went wrong in read thread: {err}")
            break
    gnss_msg_log.close()
    logging.debug("GNSS data reading thread ended")


def forward_gnss(stream, _serial_lock, stopevent, inqueue):
    """
    THREADED
    Reads RTCM3 data from message queue and sends it to receiver.
    """

    while not stopevent.is_set():
        try:
            raw_data, parsed_data = inqueue.get(timeout=1)   # block until data comes in
            if protocol(raw_data) == RTCM3_PROTOCOL:
                if LOG_GNSS_FULL:
                    logging.debug(parsed_data)
                else:
                    logging.debug(
                        f"NTRIP>> {parsed_data.identity} {RTCM_MSGIDS[parsed_data.identity]}"
                    )
                _serial_lock.acquire()
                stream.write(raw_data)
                _serial_lock.release()

                if parsed_data.identity == "1074":
                    logging.info(f"GPS RTCM3 correction data forwarded")
        except Empty as err:
            pass
        except Exception as err:
            logging.error(f"Something went wrong in forwarding thread: {err}")
            break
    logging.debug("GNSS data forwarding thread ended")

def ntrip_client(msg_queue, stopevent):
    while not stopevent.is_set():
        try:
            with GNSSNTRIPClient(None, verbosity=VERBOSITY_LOW) as gnc:
                streaming = gnc.run(
                    server=_cfg["ntrip"]["address"],
                    port=_cfg["ntrip"]["port"],
                    mountpoint=_cfg["ntrip"]["mount_point"],
                    user=_cfg["ntrip"]["username"],
                    password=_cfg["ntrip"]["password"],
                    reflat=_cfg["ntrip"]["reflat"],
                    reflon=_cfg["ntrip"]["reflon"],
                    refalt=_cfg["ntrip"]["refalt"],
                    refsep=_cfg["ntrip"]["refsep"],
                    ggamode=_cfg["ntrip"]["gga_mode"],
                    ggainterval=_cfg["ntrip"]["gga_interval"],
                    output=msg_queue,
                )
                # loop to hold the client
                while streaming and not stopevent.is_set():
                    #print(f"GNC: {gnc.connected}")
                    if not gnc.connected:
                        raise Exception("NTRIP connection closed unexpectedly")
                    sleep(1)
        except Exception as err:
            logging.error(f"Connection to NTRIP caster failed: {err}. Retrying...")
            sleep(1)
            pass


############# IMU related functions ################

def accl_raw2G(a_raw):
    if ACCL_RANGE_MODE == ACCL_RANGE_2G:
        return a_raw / 16384.0
    elif ACCL_RANGE_MODE == ACCL_RANGE_4G:
        return a_raw / 8192.0
    elif ACCL_RANGE_MODE == ACCL_RANGE_8G:
        return a_raw / 4096.0
    else:
        return a_raw / 2048.0  # +-16G

def accl_raw2ms(a_raw):
    return accl_raw2G(a_raw) * GA

def accl_raw2ms_cal3(ax_raw, ay_raw, az_raw):
    """
    Convert raw acceleration data into m/s^2 with calibration
    """
    bias = _cfg["imu"]["acc_bias"]

    ax_cal = ax_raw - bias[0]
    ay_cal = ay_raw - bias[1]
    az_cal = az_raw - bias[2]

    ax_ms2 = accl_raw2ms(ax_cal)
    ay_ms2 = accl_raw2ms(ay_cal)
    az_ms2 = accl_raw2ms(az_cal)

    return ax_ms2, ay_ms2, az_ms2

def gyro_raw2deg(g_raw):
    if GYRO_RANGE_MODE == GYRO_RANGE_250DPS:
        return g_raw / 131.0
    elif GYRO_RANGE_MODE == GYRO_RANGE_500DPS:
        return g_raw / 65.5
    elif GYRO_RANGE_MODE == GYRO_RANGE_1000DPS:
        return g_raw / 32.8
    else:
        return g_raw / 16.4    # 2000dps

def gyro_raw2rad(g_raw):
    return gyro_raw2deg(g_raw) * DEG2RAD

def gyro_raw2rad_cal3(gx_raw, gy_raw, gz_raw):
    """
    Convert raw gyro data into rad/s with calibration
    """
    bias = _cfg["imu"]["gyr_bias"]

    gx_cal = gx_raw - bias[0]
    gy_cal = gy_raw - bias[1]
    gz_cal = gz_raw - bias[2]

    gx_rad = gyro_raw2rad(gx_cal)
    gy_rad = gyro_raw2rad(gy_cal)
    gz_rad = gyro_raw2rad(gz_cal)

    return gx_rad, gy_rad, gz_rad

def mag_raw2uT(m_raw):
    return m_raw * 0.15

def mag_raw2uT_cal3(mx_raw, my_raw, mz_raw):
    bias = _cfg["imu"]["mag_bias"]
    mx_cal = mx_raw - bias[0]
    my_cal = my_raw - bias[1]
    mz_cal = mz_raw - bias[2]

    mx_uT = mag_raw2uT(mx_cal)
    my_uT = mag_raw2uT(my_cal)
    mz_uT = mag_raw2uT(mz_cal)

    return mx_uT, my_uT, mz_uT


def cal_gyro(imu):
    """
    Measure bias of gyro by taking the average of a certain number of data points
    """
    points = 500
    count = 0
    gx_sum = gy_sum = gz_sum = 0

    while count < points:
        if imu.dataReady():
            imu.getAgmt()
            gx_sum += IMU.gxRaw
            gy_sum += IMU.gyRaw
            gz_sum += IMU.gzRaw
            count += 1
        sleep(0.01)

    return [int(gx_sum / points), int(gy_sum / points), int(gz_sum / points)]

def read_imu(imu, stopevent):

    prev_time = time()
    #cx_ri = cy_ri = cz_ri = 0
    cx_mi = cy_mi = cz_mi =  0
    gx_p = gy_p = gz_p = 0
    gyr_bias = _cfg["imu"]["gyr_bias"]

    while not stopevent.is_set():
        if imu.dataReady():
            imu.getAgmt()
            cur_time = time()
            dt = cur_time - prev_time
            #logging.debug(f"[IMU] dt={dt} ax={imu.axRaw:06d} ay={imu.ayRaw:06d} az={imu.azRaw:06d} "\
            #             f"gx={IMU.gxRaw:06d} gy={IMU.gyRaw:06d} gz={IMU.gzRaw:06d} "\
            #             f"mz={IMU.mzRaw:06d} my={IMU.myRaw:06d} mz={IMU.mzRaw:06d}")

            _sensors_data_lock.acquire()
            _sensors_data_latest["syst"] = datetime.now()
            ax_raw = _sensors_data_latest["ax_raw"] = IMU.axRaw
            ay_raw = _sensors_data_latest["ay_raw"] = IMU.ayRaw
            az_raw = _sensors_data_latest["az_raw"] = IMU.azRaw
            gx_raw = _sensors_data_latest["gx_raw"] = IMU.gxRaw
            gy_raw = _sensors_data_latest["gy_raw"] = IMU.gyRaw
            gz_raw = _sensors_data_latest["gz_raw"] = IMU.gzRaw
            mx_raw = _sensors_data_latest["mx_raw"] = IMU.mxRaw
            my_raw = _sensors_data_latest["my_raw"] = IMU.myRaw
            mz_raw = _sensors_data_latest["mz_raw"] = IMU.mzRaw

            # Convert accel data
            ax_ms2_cal, ay_ms2_cal, az_ms2_cal = accl_raw2ms_cal3(ax_raw, ay_raw, az_raw)
            _sensors_data_latest["ax_ms2_cal"] = ax_ms2_cal
            _sensors_data_latest["ay_ms2_cal"] = ay_ms2_cal
            _sensors_data_latest["az_ms2_cal"] = az_ms2_cal

            # Test integrating gyro data
            gx_cal, gy_cal, gz_cal = gyro_raw2rad_cal3(gx_raw, gy_raw, gz_raw)
            #cx_ri += gx_cal * dt
            #cy_ri += gy_cal * dt
            #cz_ri += gz_cal * dt
            cx_mi += (gx_cal + gx_p) / 2 * dt  # mid-point rule
            cy_mi += (gy_cal + gy_p) / 2 * dt
            cz_mi += (gz_cal + gz_p) / 2 * dt 
            gx_p, gy_p, gz_p = gx_cal, gy_cal, gz_cal
            _sensors_data_latest["gx_rad_cal"] = gx_cal
            _sensors_data_latest["gy_rad_cal"] = gy_cal
            _sensors_data_latest["gz_rad_cal"] = gz_cal

            # Convert magnetometer data
            mx_uT_cal, my_uT_cal, mz_uT_cal = mag_raw2uT_cal3(mx_raw, my_raw, mz_raw)
            _sensors_data_latest["mx_uT_cal"] = mx_uT_cal
            _sensors_data_latest["my_uT_cal"] = my_uT_cal
            _sensors_data_latest["mz_uT_cal"] = mz_uT_cal

            _sensors_data_lock.release()
            if _sensors_data_latest["gnss_new_data"] == 1:
                # temporary show the current local rotation (as reference)
                logging.info(f"Local rotation (x,y,z): ({cx_mi * RAD2DEG:.2f}, {cy_mi * RAD2DEG:.2f}, {cz_mi * RAD2DEG:.2f})")


            _imu_new_data_evt.set()         # signal positioning thread to work on new data
            _imu_new_data_log_evt.set()     # signal logging thread to log a new row

            prev_time = cur_time
            _sensors_log_done.wait() # wait until logging is done
            # TODO: block until positioning is done

    logging.debug("IMU thread ended")

def sensors_logger(stopevent):
    """
    THREADED
    """

    with open(SENSOR_LOG_FILENAME, 'w') as csv_file:
        logging.info(f"Sensors log will be saved to: {SENSOR_LOG_FILENAME}")
        writer = csv.DictWriter(csv_file, fieldnames=list(_sensors_data_latest.keys()))
        writer.writeheader()
        _sensors_log_done.set()

        while not stopevent.is_set():
            _imu_new_data_log_evt.wait()
            _sensors_data_lock.acquire()

            writer.writerow(_sensors_data_latest)


            # clear GNSS data to save storaage
            # TODO: remove this after positioning is added
            if _sensors_data_latest["gnss_new_data"] == 1:
                _sensors_data_latest["gnss_new_data"] = 0   # mark later logged GNSS data as old
                for key in ("gnsst", "lat", "lon", "alt_msl", "alt_hae", "quality", "hdop", "diff_age", "sv_num", "gs", "cogt", "cogm"):
                    _sensors_data_latest[key] = None
            _sensors_data_lock.release()
            _imu_new_data_log_evt.clear()
            _sensors_log_done.set()
        logging.debug("Sensor logger thread ended")
        return
    
    logging.error("Failed to create sensor log file")
    return False


###### Main function #########

if __name__ == "__main__":

    parse_config_arg()

    # setup logger
    log_fn = datetime.now().strftime("rtk_imu_fusion_%Y%m%d_%H_%M_%S.log")
    if LOG_TO_FILE:
        logging.basicConfig(filename=log_fn, filemode="w", format=LOGGING_FORMAT, level=LOGGING_LEVEL)
    else:
        logging.basicConfig(format=LOGGING_FORMAT, level=LOGGING_LEVEL)
    logging.info(f"{_cfg['test_info']['test_name']} ({_cfg['test_info']['test_code']}) is going to start...")

    # init IMU and the IMU data collection thread
    logging.info("Connecting to IMU...")
    IMU = qwiic_icm20948.QwiicIcm20948()
    if IMU.connected == False:
        logging.critical("Cannot connect to IMU!")
        exit()
    IMU.begin()
    IMU.setFullScaleRangeAccel(ACCL_RANGE_MODE)
    IMU.setFullScaleRangeGyro(GYRO_RANGE_MODE)
    IMU.enableDlpfAccel(ACCL_DLPF_ENABLE)
    IMU.enableDlpfAccel(GYRO_DLPF_ENABLE)
    logging.info("Calibrating gyroscope. Keep the IMU still...")
    _cfg["imu"]["gyr_bias"] = cal_gyro(IMU)
    logging.info(f"Accel bias: {_cfg['imu']['acc_bias']} Gyro bias: {_cfg['imu']['gyr_bias']}, Mag bias: {_cfg['imu']['mag_bias']}")

    logging.debug("Starting IMU reading thread...")
    thread_imu = Thread(target=read_imu, args=(IMU, _ths_stop))
    thread_imu.start()

    # Start serial and GNSS
    try:
        serial_port = _cfg["gnss"]["serial_port"]
        baudrate = _cfg["gnss"]["baudrate"]
        timeout = _cfg["gnss"]["timeout"]
        logging.info(f"Connecting to serial port {serial_port} @ {baudrate}...")
        with Serial(serial_port, baudrate, timeout=timeout) as serial:
            logging.debug("Starting GNSS reading thread...")
            thread_gnss_read = Thread(
                target=read_gnss,
                args=(
                    serial,
                    _serial_lock,
                    _ths_stop
                ),
                daemon=True
            )
            thread_gnss_read.start()

            logging.debug("Starting GNSS data forwarding thread...")
            thread_gnss_forward = Thread(
                target=forward_gnss,
                args=(
                    serial,
                    _serial_lock,
                    _ths_stop,
                    _ntrip_queue,
                ),
                daemon=True
            )
            thread_gnss_forward.start()

            logging.info("Starting sensors data logger thread...")
            thread_sensors_logger = Thread(
                target=sensors_logger,
                args=(_ths_stop,),
                daemon=True
            )
            thread_sensors_logger.start()

            if _cfg["ntrip"]["enable"]:
                ntrip_server = _cfg["ntrip"]["address"]
                ntrip_port = _cfg["ntrip"]["port"]
                logging.info(f"Starting NTRIP client on {ntrip_server}:{ntrip_port} ...")
                thread_ntrip_client = Thread(
                    target=ntrip_client,
                    args=(
                        _ntrip_queue,
                        _ths_stop
                    ),
                    daemon=True
                )
                thread_ntrip_client.start()
            else:
                logging.warning("NTRIP client disabled!")

            # Main loop
            while not _ths_stop.is_set():  # run until user presses CTRL-C
                # press enter to increase a counter marker
                value = input()
                _sensors_data_lock.acquire()
                _sensors_data_latest["marker"] += 1
                marker = _sensors_data_latest["marker"]
                _sensors_data_lock.release()
                logging.info(f"New marker: {marker}")
                #sleep(1)
    except KeyboardInterrupt:
        _ths_stop.set()
        logging.debug("Keyboard interrupt! Stopping...")
    except Exception as err:
        _ths_stop.set()
        print(err)
    sleep(1)
