"""
Functions related to geometry
"""

import numpy as np

def intersection_pt_line(p0, lp1, lp2):
    """
    Compute the intersection point between a point and a line
    
    args:
        p0 (np.ndarray): the vector (x, y) of the point
        lp1, lp2 (np.ndarray): the vector (x, y) of the 2 points forming the line

    return:
        (np.ndarray) x, y coordinate of the intersection
    """

    # figure out the line equation L of the path formed by wp1 and wp2
    # y = mx + c
    m_path = (lp2[1] - lp1[1]) / (lp2[0] - lp1[0])
    c_path = -m_path * lp1[0] + lp1[1]

    # figure out the equation of the perpendicular line between L and p0
    m_p0 = - 1/m_path
    c_p0 = p0[1] - m_p0 * p0[0]

    # intersection point between 2 lines
    # m_p0 * x_int +c_p0 = m_path * x_int + c_path
    # x_int = (c_path - c_p0) / (m_p0 - m_path)
    x_int = (c_path - c_p0) / (m_p0 - m_path)
    y_int = m_path * x_int + c_path

    return np.array([x_int, y_int])

def cte_ne(p0, wp1, wp2):
    """
    Compute cross-track error between a point and a straight path formed by 2 way points

    Reference: https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points

    args:
        p0 (np.ndarray): the vector (n, e) of the point
        wp1, wp2 (np.ndarray): the vector (n, e) of the 2 way points forming the path line

    return:
        (float) distance between the point and the path
    """

    nom = np.abs((wp2[0] - wp1[0]) * (wp1[1] - p0[1]) - (wp1[0] - p0[0]) * (wp2[1] - wp1[1]))
    denom = np.linalg.norm([wp2[0] - wp1[0], wp2[1] - wp1[1]])
    cte = nom / denom

    return cte

def alt_error_ned(p0, wp1, wp2):
    """
    Compute altitude error between a point and a straight path formed by 2 way points

    Ground truth altitude will be treated as the linearly interpolated altitude
    of the projected point on the line.

    If the projected point is outside the range between wp1 and wp2, the altitude of the
    closer point will be used.

    args:
        p0 (np.ndarray): the vector (n, e, d) of the point
        wp1, wp2 (np.ndarray): the vector (n, e, d) of the 2 way points forming the path line

    return:
        (float) distance between the point and the path. Positive value means the point is
        higher than the ground truth
    """

    p0_xy = np.array([p0[1], p0[0]])
    wp1_xy = np.array([wp1[1], wp1[0]])
    wp2_xy = np.array([wp2[1], wp2[0]])
    intersect = intersection_pt_line(p0_xy, wp1_xy, wp2_xy)

    dist_wp1_wp2 = np.linalg.norm([wp2_xy[0] - wp1_xy[0], wp2_xy[1] - wp1_xy[1]])
    dist_wp1_int = np.linalg.norm([intersect[0] - wp1_xy[0], intersect[1] - wp1_xy[1]])
    dist_int_wp2 = np.linalg.norm([intersect[0] - wp2_xy[0], intersect[1] - wp2_xy[1]])

    if dist_int_wp2 > dist_wp1_wp2:     # intersection is outside wp1
        gt_alt = wp1[2]
    elif dist_wp1_int > dist_wp1_wp2:   # outside wp2
        gt_alt = wp2[2]
    else:   # within the range
        gt_alt = (dist_wp1_int / dist_wp1_wp2) * (wp1[2] + wp2[2])

    return gt_alt - p0[2]