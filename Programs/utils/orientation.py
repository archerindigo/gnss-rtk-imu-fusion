"""
Functions related to orientation operations
"""

import numpy as np

_GA = 9.80665

def am2rpy(a, m, deg: bool=False) -> np.ndarray:

    """
    Find roll, pitch, yaw angle from accelerometer and magnetometer readings

    args:
        a:  Acceleration in front, right, down axes in m/s^2
        m:  Magnetic field strength in roll, pitch, yaw axes (unit doesn't matter but need to be consistance)
        deg: If true, return the angles in degrees. Otherwise, in radian

    return:
        numpy.ndarray containing the roll, pitch, yaw angles
    """

    p_rad = np.arcsin(a[0]/ _GA)
    r_rad = -np.arcsin(a[1]/ _GA * np.cos(p_rad))
    Bxc = m[0] * np.cos(p_rad) + (m[1] * np.sin(p_rad) + m[2] * np.cos(r_rad)) * np.sin(p_rad)
    Byc = m[1] * np.cos(r_rad) - m[2] * np.sin(p_rad)
    y_rad = -np.arctan2(Byc, Bxc)

    ypr = np.array([r_rad, p_rad, y_rad])

    return np.deg2rad(ypr) if deg else ypr

def rpy2quat(rpy) -> np.ndarray:
    """
    Convert Euler angles to quaternion

    It follows the yaw-pitch-roll rotation sequence

    Reference: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Euler_angles_(in_3-2-1_sequence)_to_quaternion_conversion
    """
    cy = np.cos(rpy[2] * 0.5)
    sy = np.sin(rpy[2] * 0.5)
    cp = np.cos(rpy[1] * 0.5)
    sp = np.sin(rpy[1] * 0.5)
    cr = np.cos(rpy[0] * 0.5)
    sr = np.sin(rpy[0] * 0.5)

    w = cr * cp * cy + sr * sp * sy
    x = sr * cp * cy - cr * sp * sy
    y = cr * sp * cy + sr * cp * sy
    z = cr * cp * sy - sr * sp * cy

    return np.array([w, x, y, z])

def rpy2R(rpy) -> np.ndarray:
    '''
    Comput rotation matrix from rpy

    The rotation is in yaw, pitch, roll order
    '''
    sy = np.sin(rpy[2])
    cy = np.cos(rpy[2])
    sp = np.sin(rpy[1])
    cp = np.cos(rpy[1])
    sr = np.sin(rpy[0])
    cr = np.sin(rpy[0])
    m00 = cp * cy
    m01 = cy * sp * sr - sy * cr
    m02 = cy * sp * cr + sy * sr
    m10 = sy * cp
    m11 = sy * sp * sr + cy * cr
    m12 = sy * sp * cr - cy * sr
    m20 = -sp
    m21 = sp * sr
    m22 = cp * cr
    return np.array([[m00, m01, m02], [m10, m11, m12], [m20, m21, m22]])

def quat2dcm(q) -> np.ndarray:
    """
    Convert quaternion to direct cosine matrix
    Reference:
    - https://www.mathworks.com/help/aeroblks/quaternionstodirectioncosinematrix.html
    - https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Rotation_matrices
    - J. Sola, “Quaternion kinematics for the error-state Kalman ﬁlter”. P.25

    In theory it is the homogeneous form of rotation matrix. If the quaternion is not normalized,
    the DCM is still a scalar multiple of a rotation matrix.
    """
    R = np.empty((3,3))

    R[0][0] = q[0]*q[0] + q[1]*q[1] - q[2]*q[2] - q[3]*q[3]
    R[1][1] = q[0]*q[0] - q[1]*q[1] + q[2]*q[2] - q[3]*q[3]
    R[2][2] = q[0]*q[0] - q[1]*q[1] - q[2]*q[2] + q[3]*q[3]

    R[0][1] = 2 * (q[1]*q[2] + q[0]*q[3])
    R[0][2] = 2 * (q[1]*q[3] - q[0]*q[2])

    R[1][0] = 2 * (q[1]*q[2] + q[0]*q[3])
    R[1][2] = 2 * (q[2]*q[3] - q[0]*q[1])
    #R[1][0] = 2 * (q[1]*q[2] - q[0]*q[3])  # mathworks' definition, maybe wrong
    #R[1][2] = 2 * (q[2]*q[3] + q[0]*q[1])  # mathworks' definition, maybe wrong
    
    R[2][0] = 2 * (q[1]*q[3] - q[0]*q[2])
    R[2][1] = 2 * (q[0]*q[1] + q[2]*q[3])
    #R[2][0] = 2 * (q[1]*q[3] + q[0]*q[2])  # mathworks' definition, maybe wrong
    #R[2][1] = 2 * (q[2]*q[3] - q[0]*q[1])  # mathworks' definition, maybe wrong

    return R

def quat2R(q) -> np.ndarray:
    """
    Convert a quaternion to rotation matrix (inhomogeneous form)
    Reference:
    - https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Rotation_matrices
    """
    R = np.empty((3,3))
    R[0][0] = 1 - 2 * (q[2]*q[2] + q[3]*q[3])
    R[1][1] = 1 - 2 * (q[1]*q[1] + q[3]*q[3])
    R[2][2] = 1 - 2 * (q[1]*q[1] + q[2]*q[2])

    R[0][1] = 2 * (q[1]*q[2] - q[0]*q[3])
    R[0][2] = 2 * (q[1]*q[3] + q[0]*q[2])

    R[1][0] = 2 * (q[1]*q[2] + q[0]*q[3])
    R[1][2] = 2 * (q[2]*q[3] - q[0]*q[1])

    R[2][0] = 2 * (q[1]*q[3] - q[0]*q[2])
    R[2][1] = 2 * (q[2]*q[3] + q[0]*q[1])

    return R


def quatmul(p, q) -> np.ndarray:
    """
    Quaternion multiplication
    """
    w = p[0]*q[0] - p[1]*q[1] - p[2]*q[2] - p[3]*q[3]
    x = p[0]*q[1] + p[1]*q[0] + p[2]*q[3] - p[3]*q[2]
    y = p[0]*q[2] - p[1]*q[3] + p[2]*q[0] + p[3]*q[1]
    z = p[0]*q[3] + p[1]*q[2] - p[2]*q[1] + p[3]*q[0]

    return np.array([w, x, y, z])

def quat2rpy(q, deg: bool=False) -> np.ndarray:

    # roll
    sinr_cosp = 2.0 * (q[0]*q[1] + q[2]*q[3])
    cosr_cosp = 1.0 - 2.0 * (q[1]*q[1] + q[2]*q[2])
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    # pitch
    sinp = 2.0 * (q[0]*q[2] - q[1]*q[3])
    if np.abs(sinp) >= 1:
        pitch = np.copysign(np.pi / 2.0, sinp)
    else:
        pitch = np.arcsin(sinp)    

    # yaw
    siny_cosp = 2.0 * (q[1]*q[2] + q[0]*q[3])
    cosy_cosp = 1.0 - 2.0 * (q[2]*q[2] + q[3]*q[3])
    yaw = np.arctan2(siny_cosp, cosy_cosp)

    rpy = np.array([roll, pitch, yaw])

    return np.rad2deg(rpy) if deg else rpy

def skew_mat(w) -> np.ndarray:
    """
    Return a skew symmetric matrix from a given vector

    w: A 3x1 vector
    """

    return np.array([[0., -w[2], w[1]],
                     [w[2], 0., w[0]],
                     [w[1], w[0], 0.]])
