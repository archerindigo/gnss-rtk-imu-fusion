import qwiic_icm20948
import time

######## Constants #########
ACCL_RANGE_2G = 0x00
ACCL_RANGE_4G = 0x01
ACCL_RANGE_8G = 0x02
ACCL_RANGE_16G = 0x03
GYRO_RANGE_250DPS = 0x00
GYRO_RANGE_500DPS = 0x01
GYRO_RANGE_1000DPS = 0x02
GYRO_RANGE_2000DPS = 0x03

# IMU parameters
ACCL_RANGE_MODE = ACCL_RANGE_4G
ACCL_DLPF_ENABLE = False
GYRO_RANGE_MODE = GYRO_RANGE_500DPS
GYRO_DLPF_ENABLE = False

if __name__ == "__main__":
    IMU = qwiic_icm20948.QwiicIcm20948()
    if IMU.connected == False:
        print("Cannot connect to IMU!")
        exit()

    IMU.begin()
    IMU.setFullScaleRangeAccel(ACCL_RANGE_MODE)
    IMU.setFullScaleRangeGyro(GYRO_RANGE_MODE)
    IMU.enableDlpfAccel(ACCL_DLPF_ENABLE)
    IMU.enableDlpfAccel(GYRO_DLPF_ENABLE)

    start_time = time.time()
    while True:
        if IMU.dataReady():
            IMU.getAgmt()
            run_time = time.time() - start_time
            print(f"[IMU] rt={run_time:.3f} ax={IMU.axRaw:06d} ay={IMU.ayRaw:06d} az={IMU.azRaw:06d} "\
                  f"gx={IMU.gxRaw:06d} gy={IMU.gyRaw:06d} gz={IMU.gzRaw:06d} "\
                  f"mz={IMU.mzRaw:06d} my={IMU.myRaw:06d} mz={IMU.mzRaw:06d}")

            time.sleep(0.01)
