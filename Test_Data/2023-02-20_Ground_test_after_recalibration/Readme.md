2023-02-23 Ground Test after Recalibration
===

Objective:

- Gather new data sets after re-calibrating the magnetometer
- Test static accuracy without RTK

Tests:

1. Static test at starting point with RTK
2. Static test at starting point without RTK
3. Dynamic test

Cautions:

- Reset the GNSS at the beginning of each test
- Check the heading before the test
- Check whether the sensor data are valid
   - Correct scale of IMU
   - Reasonable magnetic field strength

## Initial data:

Starting point coordinate (Google Maps): 48.0145094, 7.8324844

Starting height (HAE): 297.05 +- 0.1m (from RTK mode)

Starting Heading: 192° (according to mobile compass)

Assumed accelerometer bias: [-29, -77, -91, -108, 93, 93] () 

Assumed initial magnetometer bias: [21, 5, -187] (tested on the same day with the same component placement)

## Notes on Data Sets

- The magnetometer data was not correctly biased so the values in uT as recorded in the csv files are not true
- Bias values might be different with the assumed one as the test environment was different
- Even though the gyroscope bias was computed at the beginning of the test, the data might not perfectly unbiased.
- Console output log of the tests are lost

## Static Test non-RTK 1

Test code: static_noRTK_0220_1

Time to reach fix mode: ~136 s

Fix mode pos (offset not applied): 48.01450498, 7.83248061

Fix mode pos (offset applied): 48.01450138, 7.83247511

Non-RTK mode mean:

Fix mode coordinate match with GIS?

## Dynamic Test 1

The position in the first lap was quite inaccurate as GNSS was in float mode during the hole lap.

Once it got to fix mode, it is super accurate again.

fig/dyn_0220_1/03-20: Analysis with attempt to predict the position through gyroscope estimated heading only and not to rely on the accelerometer

fig/dyn_0220_1/04-07: Normal analysis with a specific set of parameters

### Dynamic Test 2

Time to reach fixed: ~210s

The test started after fix mode is reached.

GPS position looks very accurate, but the gyroscope suffer from drift a lot.

fig/dyn_0220_2/04-07: Normal analysis with a specific set of parameters

### Dynamic non-RTK Test 1

Test without RTK.

marker 1: start of the test

Note: 2 cars passed by in the 3rd leg of the final lap

fig/dyn_noRTK_0220_1/04-07: Normal analysis with a specific set of parameters
