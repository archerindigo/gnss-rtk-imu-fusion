## Data for IMU Calibration

Do not trust mx_uT, my_uT, mz_uT in the sensor log. They're wrong!

Check the jupyter notes for calibration process

Update 18/2/2023: The layout of the test setup was changed. So the magnetometer data gathered here are not valid for calibration anymore.

## Test data

`senssor_data_a*.csv`: Data for zero-g bias calibration in each axis and each sign

`sensor_data_mag*.csv`: Data for calibration on magnetometer

`sensor_data_motion*.csv`: IMU rotated around the yaw axis. For testing different attitude filters.
