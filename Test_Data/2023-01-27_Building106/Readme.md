# 2023-01-27 Building 106 Field Tests

## General Comments of the Tests

- The starting point of the ground test may not give good satellite reception. Next time I should find a better spot and confirm the GNSS accuracy with static tests first
- The ground test generally contain 2-5m offset to the south. Maybe due to the problem of the starting point
- The number of satellite in used reported by GNGGA sentence won't go above 12, while u-center can resolve to higher number. Maybe I should find out how to look-up the correct number of satellite in use, or just treat 12 as good.

### Ground Specification

Size of each car slot: 2.5 x 5.2 m

Width of line: 0.14 m

### RTK Ground Test Static

- Static test using u-center
- RTK status were in float most of the time
- Deviation is high (up to 4 m)

### RTK Ground Test 1

- Stayed static at starting point for 7 mins
- Deviation is even higher than the u-center static test (up to 10 m)
- Only 9-10 satellites were used most of the time which is fewer than other tests (19 used in u-center static test for instance)

## RTK Ground Test 2

- A short route was followed
- About 5m offset to the south

### RTK Ground Test 3

- Stayed at the starting point for a while and then follow the planned path
- The dynamic trace looks good
- About 5m offset to the south and 1 m to the east
- In the final legs the GNSS path can follow closely with the actual path

### RTK Ground Test 4

- Similar to Test 3

- About 2.5m offset to the south throughout the whole test

### Non-RTK Ground Test 1

- Looks more accurate than RTK tests

### Non-RTK Ground Test 2

- About 2.5m offset to the south throughout the whole test

### Non-RTK Ground Test 3

- Starting point off to northeast instead of south
- In the final legs the GNSS path could follow the actual path, but not as precise as RTK Ground Test 3

### Air Test 1-4

- Antenna faced downward and RTK status never got to float or above
- Test 4 is static test. Deviation is up to 10 m

### Air Test 5

- Stayed static for 5 mins 45 secs with antenna put to the top
- RTK status stayed at float almost all the time
- Deviation is within 0.5 m

### Air Test 6

- Normal flight
- Shape of the GPS flight path looks good

### Air Test 7

- Random flight with some circular motions
