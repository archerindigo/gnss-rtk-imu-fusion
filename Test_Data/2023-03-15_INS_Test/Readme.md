2023-03-16 Dead-reckoning Tests
===

Today's tests are to check the attitude and motion estimation through dead reckoning only

## Pre-test Data

Acceleromter bias: [-53, -10, 93] (Used the old value obtained from the calibration in Feb)

Hard-bias used for magnetometer: [87, 97, -108] (Obtained from the day of the test)

## 0316_1

Motion sequence:

1. Began from heading ~220° (checked with phone's compass) on a flat surface
2. Rotates 360° clockwise, hold for a few seconds and then rotate back
3. Rolls to the right for 180°, hold for a few seconds and then roll back
4. Rolls to the left for 180°, hold for a few seconds and then roll back
5. Pitches up for 90°, and then comes back
6. Pitches down for 90°, and then comes back

Observations:

- Large initial yaw error (got 282.5°)

## 0316-2

Motion sequence:

1. Began from heading ~100° (checked with phone's compass) on a flat surface
2. Pushes forward and then backward
3. Slides to the right and then slides back

## Comment on the Analysis

High acceleration biases observed. Hard-coding the initial attitude doesn't help.
