## Calibrations for Accelerometer and Magnetometer; Attitude Estimators Tests

## Background

rtk_fusion.py had a lot of fixs and revamp over the past 2 days. The tests here are to re-calibrate the magnetometer, test the adoption of the new sensor log format and have some motion tests to find out the directional behaviors of the IMU.

## Objectives

- Accelerometer Calibration
   - Find out the bias in each axis
   - Find out the variances in each axis
- Magnetometer calibration
   - Find out the bias in each axis
- Motion test
   - Find out the way to correctly resolve accelerations in NED frame
   - Check the performance of different attitude estimators:
      - EKF
      - Madgwick
      - e-compass

## Test 1: Accelerometer Calibration

This experiment is to find out the bias of accelerometer and the variance

`calibration_acc.ipynb` contains the analysis script.

### Setup

![setup_acc_cal.JPG](pic/setup_acc_cal.JPG)

1. The RPi was mounted on a tripod. Target axis was set vertically to the ground (validated by the level indicator)
2. Raw data were captured for about 10s on each axis

### Results

**Bias data found:**

| axis | mean bias (LSB) | mean bias (m/s^2) | variance (m/s^2) | s.d. (m/s^2) |
| ---- | --------------- | ----------------- | ---------------- | ------------ |
| +x   | -29             | -0.03471592       | 0.003858         | 0.06211      |
| -x   | -77             | -0.09217676       | 0.003490         | 0.05908      |
| +y   | -91             | -0.10893618       | 0.003650         | 0.06042      |
| -y   | -108            | -0.12928689       | 0.003443         | 0.05868      |
| +z   | 93              | 0.11133038        | 0.003451         | 0.05874      |
| -z   | (93)            | (0.11133038)      | (0.003451)       | (0.05874)    |

-ve z was not tested as I couldn't fix the device in that direction. Assume to be the same as +ve z axis

**Zero-gravity static error distribution after calibration:**

The error distributions are quite normal

![acc_axn.png](fig/acc_axn.png)

![acc_axp.png](fig/acc_axp.png)

![acc_ayn.png](fig/acc_ayn.png)

![acc_ayp.png](fig/acc_ayp.png)

![acc_azp.png](fig/acc_azp.png)

### Test 2: Magnetometer Calibration

This experiment is to find out the bias of the magnetometer. Only hard-iron calibration was performed.

`calibration_mag.ipynb` contains the analysis script.

### Setup

![setup_mag_cal.JPG](pic/setup_mag_cal_0220.JPG)

The RPi was mounted on the basket and then rotated in all directions.

**This setup is the same when doing the ground test on 20/2. If the setup is changed, re-calibration is required.**

### Results

Using dataset `sensor_data_mag_7`, the hard-iron bias is found to be:

| axis | bias in LSB | corresponding uT |
| ---- | ----------- | ---------------- |
| x    | 25          | 3.15             |
| y    | 5           | 0.75             |
| z    | -187        | -28.05           |

Mean absolute magnitude after calibration = 45.88 uT, which is in the range of earth magnetic field strength

### Figures

![mag_before_cal_absolute_7.png](fig/mag_before_cal_absolute_0220_2.png)

![mag_before_cal_2d_7.png](fig/mag_before_cal_2d_0220_2.png)

![mag_before_cal_3d_7.png](fig/mag_before_cal_3d_0220_2.png)

![mag_before_cal_absolute_7.png](fig/mag_before_cal_absolute_0220_2.png)

![mag_after_cal_2d_7.png](fig/mag_after_cal_2d_0220_2.png)

![mag_after_cal_3d_7.png](fig/mag_after_cal_3d_0220_2.png)

## Test 3: Motion Test

It is to check the performance of different attitude estimators, namely Extended Kalman Filter, Madgwick Filter and e-compass.

It is also necessary to check whether the estimators can find out the correct heading and motion in NED frame.

### Setup

![setup_yaw.JPG](pic/setup_yaw.JPG)

### Procedure

motion_1:

1. Point the setup to the south

2. Rotate 360° clockwise

3. Rotate 360° anticlockwise

4. Pitch up

5. Pitch down

6. move upward, then come back

7. move northwards, then back to original position

8. move eastwards, ...
