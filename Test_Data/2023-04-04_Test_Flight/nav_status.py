# Plot navigation status for the drone flying test data set with u-center

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pymap3d as pm

plt.rcParams.update({'font.size': 12})

filename = "raw/tableview_230412_090717.csv"  # update csv filename here

df = pd.read_csv(filename)

# Convert UTC column to datetime
df["UTC"] = pd.to_datetime(df["UTC"])

def highlight_carrier_status(ax, df):
    idxes = df[df["Carrier Range Status"].diff() != 0 ].index.tolist()
    idxes.append(df.index.max()+1)
    
    for start, end in zip(idxes, idxes[1:]):
        status = df["Carrier Range Status"][start]
        if status == 1:
            ax.axvspan(df["UTC"][start], df["UTC"][end-1], alpha=0.2, color="blue")
        elif status == 2:
            ax.axvspan(df["UTC"][start], df["UTC"][end-1], alpha=0.2, color="green")
        else:
            ax.axvspan(df["UTC"][start], df["UTC"][end-1], alpha=0.2, color="yellow")
        
        handles, labels = ax.get_legend_handles_labels()
        q2 = mpatches.Patch(color="yellow", label="Non-RTK mode")
        q5 = mpatches.Patch(color="blue", label="Float mode")
        q4 = mpatches.Patch(color="green", label="Fix mode")
        handles.extend([q2, q5, q4])
        ax.legend(handles=handles)

fig, ax = plt.subplots(1, 1, figsize=(12, 6), sharex=True)

df.plot(ax=ax,
        title="Quality mode and Satellites in Track (Test: air_0404_3)",
        x="UTC",
        y=["SVs Tracked", "SVs Used"],
        ylabel="Number of Satellites",
        xlabel="UTC time (hh:mm)",
        grid=True)

highlight_carrier_status(ax, df)

df_cs = df["Carrier Range Status"]
df_cs_total_cnt = df_cs.count()
df_cs_nofix_cnt = df_cs[df_cs == 0].count()
df_cs_float_cnt = df_cs[df_cs == 1].count()
df_cs_fix_cnt = df_cs[df_cs == 2].count()

print(f"Total no. of data points = {df_cs_total_cnt}")
print(f"Data points without carrier range availability = {df_cs_nofix_cnt} ({df_cs_nofix_cnt / df_cs_total_cnt * 100:0.2f}%)")
print(f"Data points with \"float\" carrier range status = {df_cs_float_cnt} ({df_cs_float_cnt / df_cs_total_cnt * 100:0.2f}%)")
print(f"Data points with \"fix\" carrier range status = {df_cs_fix_cnt} ({df_cs_fix_cnt / df_cs_total_cnt * 100:0.2f}%)")

plt.show()
