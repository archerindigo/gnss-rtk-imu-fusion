Test Data
===

This folder contains test data carried out on different days and some of the corresponding analysis.

The jupyter notebooks produced part of the analysis in some tests. However, the scripts are typically experimental and not guaranteed to generate proper analysis.

`../Programs/analysis.py` is a more proper script but it may not be compatible to older sensor data csv files. Please follow the readme of analysis.py to fix the csv file for analysis.

In the thesis, data from the following tests are focused:

- 2022-11-23_Seepark: RTK_1Hz; noRTK_1Hz

- 2023-02-20_Ground_test_after_recalibration: all tests

- 2023-03-27_Ground_Test_High_GNSS_Rate: dyn_0327_2

- 2023-04-04_Test_Flight: air_3
