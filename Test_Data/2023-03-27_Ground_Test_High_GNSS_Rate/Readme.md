# 2023-03-27 Ground Test with High GNSS Update Rate

## Special Condition

The reception of GLONASS satellite was found to be unstable. The satellite kept popping in and out, even when the original antenna was used. With GLONASS enabled, the GNSS module could never get to fix mode and seldom stay at float mode constantly. The disconnection of GLONASS satellite might ruined the carrier-phase measurement.

After disabling GLONASS reception, the GNSS module could get to fix mode. However, it was still not as stable as previous ground tests done in February. A possible reason maybe due to fewer number of satellite in track.

## dyn_0327_1

Tested using rtk_fusion.py. It could only stay in float mode but couldn't get to fix mode. Only 9 satellites from GPS could be tracked, which may be a reason of unable to get to fix mode.

## dyn_0327_2

Tested using u-center
