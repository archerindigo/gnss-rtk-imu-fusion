# 2023-02-07 Building 106 Field Tests

Time: 14:30 - 17:30

Weather: clear

## Focus of the test

- Get better GNSS data by moving the starting point to a more open spot

- Try a rectangular path which make the reference path easier to establish

- Measure the coordinates of a few key spots

## General Comments on the Tests

- Much better satellite reception than before. Fix mode could easily be reached
- Looking at the path trace on the map, the positioning of GNSS without RTK is less precise than the one with RTK
- The Google Maps base satellite layer has slight offset to northeast. This bias may be corrected by comparing the coordination at the Seepark reference point
- Ground Test 9 and Ground Test non-RTK 5 gives clear accuracy difference between RTK and non-RTK

## Static Test 2 - 6

- Static tests carried out with u-center for finding out the reference coordination of the 4 corners of the rectangular path

- Static test 3 at the Northeastern corner couldn't get to fix mode, but static test 4 could

- Take the mean in fix mode, the coordination of the 4 corners are:
  
   - Northwestern corner (starting point): 48.014505591954, 7.83248078482759
  
   - Northeastern corner: 48.0145035490517, 7.83261470698276
  
   - Southwestern corner: 48.0143238989, 7.83261021395
  
   - Southeastern corner: 48.014325426965, 7.83247711859922

## Ground Test 5 - 6

- Static test using custom python program

- Carried out at another spot outside the final test path, so the data may not be useful

## Ground Test 7

- A static test was first made at the starting point and then the device was travelled in a jagged path
- The trace of the GNSS position looks quite good

![googlemap_gnd_7.png](fig\googlemap_gnd_7.png)

## Ground Test non-RTK 4

- Static test at starting point without RTK

- The GNSS position is less precise

## Ground Test 8

- RTK test following the jagged path

- Larger GNSS bias to southwest is shown

![googlemap_gnd_8.png](fig\googlemap_gnd_8.png)

## Ground Test without RTK 6

- Followed the jagged path but without RTK

- The GNSS trace is less accurate and less precise than the case with RTK

![googlemap_gnd_noRTK_6.png](fig\googlemap_gnd_noRTK_6.png)

## Ground Test 9

- RTK test followed a rectangular path for 3 laps

- GNSS trace looks quite accurate and precise 

![googlemap_gnd_9.png](fig\googlemap_gnd_9.png)

## Ground Test non-RTK 5

- Non-RTK test following the rectangular path for 3 laps

- GNSS trace looks less precise than the case with RTK

![googlemap_gnd_noRTK_5.png](fig\googlemap_gnd_noRTK_5.png)

## analysis.ipynb

This notebook contain script to test out some attitude filters.

## static_analysis.ipynb

This notebook performs basic plotting to analysis the static tests.
