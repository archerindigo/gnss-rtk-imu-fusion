Summary
===

Test date: 23.11.2022

Location: Seepark GPS Reference Point

Motion: Static

Update period: 1.0 s

Reference Coordinate:

- Latitude: 48.012317°N (ΔN: 0.00001°= 1.1 m)
- Longitude: 7.813296°E (ΔE: 0.00001° = 0.8 m)
- Altitude (HAE): 289.4 (ΔA: 0.1 m)

## Results

|                                  | Test without RTK | RTK (full RTK session) | RTK (carrier-range = float) | RTK (carrier range = fix) |
| -------------------------------- | ---------------- | ---------------------- | --------------------------- | ------------------------- |
| No. of data points               | 1022 (100%)      | 952 (93.98%)           | 396 (39.09%)                | 556 (54.89%)              |
| **Absolute ECEF Distance Error** |                  |                        |                             |                           |
| 95th-percentile (m)              | 6.076            | 2.741                  | 3.326                       | 2.405                     |
| 75th-percentile (m)              | 4.464            | 1.384                  | 2.608                       | 0.057                     |
| 50th-percentile (m)              | 3.342            | 0.057                  | 1.094                       | 0.041                     |
| 25th-percentile (m)              | 3.041            | 0.041                  | 0.525                       | 0.041                     |
| **Absolute Altitude Error**      |                  |                        |                             |                           |
| 95th-percentile (m)              | 5.60             | 2.60                   | 3.30                        | 0.40                      |
| 75th-percentile (m)              | 3.60             | 0.50                   | 2.40                        | 0.00                      |
| 50th-percentile (m)              | 1.70             | 0.00                   | 0.80                        | 0.00                      |
| 25th-percentile (m)              | 0.90             | 0.00                   | 0.20                        | 0.00                      |

## Plot - Test without RTK

![pos_err.png](noRTK_1Hz/pos_err.png)

![alt_err.png](noRTK_1Hz/alt_err.png)

## Plot - RTK (Full RTK Session)

![pos_err_full.png](RTK_1Hz/pos_err_full.png)

![alt_err_full.png](RTK_1Hz/alt_err_full.png)

## Plot - RTK (Carrier range status = float)

![pos_err_float.png](RTK_1Hz/pos_err_float.png)

![alt_err_float.png](RTK_1Hz/alt_err_float.png)

## Plot - RTK (Carrier range status = fix)

![pos_err_fix.png](RTK_1Hz/pos_err_fix.png)

![alt_err_fix.png](RTK_1Hz/alt_err_fix.png)

## What's Next?

How to define the covariance matrix for the measurement of GNSS?
